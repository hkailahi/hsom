data Day = Monday | Tuesday | Wednesday |
  Thursday | Friday | Saturday | Sunday
  deriving (Eq, Ord, Show, Read, Bounded, Enum)

  -- *Main> Wednesday
  -- Wednesday
  -- *Main> show Wednesday
  -- "Wednesday"
  -- *Main> read "Saturday" :: Day
  -- Saturday
  -- *Main> Saturday == Sunday
  -- False
  -- *Main> Monday `compare` Wednesday
  -- LT
  -- *Main> minBound :: Day
  -- Monday
  -- *Main> maxBound :: Day
  -- Sunday
  -- *Main> succ Mon
  -- Monad   Monday  Monoid
  -- *Main> succ Monday
  -- Tuesday
  -- *Main> [Thursday .. Sunday ]
  -- [Thursday,Friday,Saturday,Sunday]
  -- *Main> [minBound .. maxBound ] :: [Day]
  -- [Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday]
  -- *Main>

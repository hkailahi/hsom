import qualified Data.Map as Map

-- 8.5 Type synonyms --

phoneBook :: [(String, String)]
phoneBook = [("betty", "555-2938")
            ,("bonnie", "452-2928")
            ,("patsy", "493-2928")
            ,("lucille", "205-2928")
            ,("wendy", "939-8282")
            ,("penny", "853-2492")
            ]

-- type PhoneBook = [(String, String)]

type PhoneNumber = String
type Name = String
type PhoneBook = [(Name, PhoneNumber)]

-- without type synonyms
-- inPhoneBook :: String -> String -> [(String, String)] -> Bool
-- inPhoneBook name pnumber pbook = (name, pnumber) `elem` pbook

inPhoneBook :: Name -> PhoneNumber -> PhoneBook -> Bool
inPhoneBook name pnumber pbook = (name, pnumber) `elem` pbook

-- parameterized type synonym example
-- type AssocList k v = [(k,v)]
-- now a funcion that gets a vaule by a key in an association list
-- can have a type of (Eq k) => k -> AssocList k v -> Maybe v
-- AssocList is a ype constructor that takes two types and produces a concret type
-- like AssocList Int String for instance

import qualified Shapes as S

-- 8.1 Algebraic data types intro --

-- data Bool = False | True
-- data Int = -2147483648 | -2147483648 | ... | -1 | 0 | 2 | ... | 21474836487
-- ^ int isn't actually defined like this, the elipsis are for illustrative purposes

-- data Point = Point Float Float deriving (Show)
-- data Shape = Circle Point Float | Rectangle Point Point deriving (Show)
-- data Shape = Circle Float Float Float | Rectangle Float Float Float Float deriving (Show)

-- Below is for shape = Rectangle Float Float Float Float
-- surface :: Shape -> Float
-- surface (Circle _ _ r) = pi * r ^ 2
-- surface (Rectangle x1 y1 x2 y2) = (abs $ x2 - x1) * (abs $ y2 - y1)

-- surface1 :: Shape -> Float
-- surface1 (Circle _ r) = pi * r ^ 2
-- surface1 (Rectangle (Point x1 y1) (Point x2 y2)) = (abs $ x2 - x1) * (abs $ y2 - y1)

-- old things tried on command line:
-- surface $ Circle 10 20 10
-- surface $ Rectangle 0 0 100 100
-- map (Circle 10 20) [4,5,6,6]

surface :: S.Shape -> Float
surface (S.Circle _ r) = pi * r ^ 2
surface (S.Rectangle (S.Point x1 y1) (S.Point x2 y2)) = (abs $ x2 - x1) * (abs $ y2 - y1)

-- function that nudges a Shape
nudge :: S.Shape -> Float -> Float -> S.Shape
nudge (S.Circle (S.Point x y) r) a b = S.Circle (S.Point (x+a) (y+b) ) r
nudge (S.Rectangle (S.Point x1 y1) (S.Point x2 y2)) a b = S.Rectangle (S.Point (x1+a) (y1+b)) (S.Point (x2+a) (y2+b))

baseCircle :: Float -> S.Shape
baseCircle r = S.Circle (S.Point 0 0) r

baseRect :: Float -> Float -> S.Shape
baseRect width height = S.Rectangle (S.Point 0 0) (S.Point width height)

-- new things tried on command line (these ones work after edit):
-- nudge (baseRect 40 100) 60 23
-- nudge (Circle (Point 34 34) 10) 5 10

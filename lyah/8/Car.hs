-- Non record syntax
-- data Car String String Int deriving (Show)

-- Record syntax
-- data Car = Car {company :: String, model :: String, year :: Int} deriving (Show)

-- for command line
-- Car "Ford" "Mustang" 1967
-- or
-- Car {company="Ford", model="Mustang", year=1967}
--

-- tellCar :: Car -> String
-- tellCar (Car {company = c, model = m, year = y}) =
--   "This " ++ c ++ " " ++ m ++ " was made in " ++ show y

-- ex.
-- *Main> let stang = Car {company="Ford", model="Mustang", year=1967}
-- *Main> tellCar stang
-- "This Ford Mustang was made in 1967"


data Car a b c = Car {  company :: a
                      , model :: b
                      , year :: c
                      } deriving (Show)

tellCar' :: (Show a) => Car String String a -> String
tellCar' (Car {company = c, model = m, year = y}) =
  "This " ++ c ++ " " ++ m ++ " was made in " ++ show y

-- ex.
-- *Main> let stang = Car {company="Ford", model="Mustang", year=1967}
-- *Main> tellCar' stang
-- "This Ford Mustang was made in 1967"
-- *Main> let stang = Car {company="Ford", model="Mustang", year="nineteen sixty seven"}
-- *Main> tellCar' stang
-- "This Ford Mustang was made in \"nineteen sixty seven\""

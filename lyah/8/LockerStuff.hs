import qualified Data.Map as Map

-- FROM: http://stackoverflow.com/questions/7916327/whats-the-difference-between-type-and-data-type-in-haskell
-- Type and data type refer to exactly the same concept
-- The Haskell keywords "type" and "data" are different, though:
-- "data" allows you to introduce a new algebraic data type
-- "type" just makes a type synonym

data LockerState = Taken | Free deriving (Show, Eq)
type Code = String
type LockerMap = Map.Map Int (LockerState, Code)

lockerLookup :: Int -> LockerMap -> Either String Code
lockerLookup lockerNumber map =
    case Map.lookup lockerNumber map of
        Nothing -> Left $ "Locker number " ++ show lockerNumber ++ " doesn't exist!"
        Just (state, code) -> if state /= Taken
                                then Right code
                                else Left $ "Locker " ++ show lockerNumber ++ " is already taken!"

lockers :: LockerMap
lockers = Map.fromList
  [ (100, (Taken, "ZD39I"))
  , (101, (Free, "JAH3I"))
  , (103, (Free, "IQSA9"))
  , (105, (Free, "QOTSA"))
  , (109, (Taken, "893JJ"))
  , (110, (Taken, "99292"))
  ]

-- ex.
-- *Main> lockerLookup 101 lockers
-- Right "JAH3I"
-- *Main> lockerLookup 100 lockers
-- Left "Locker 100 is already taken!"
-- *Main> lockerLookup 102 lockers
-- Left "Locker number 102 doesn't exist!"
-- *Main> lockerLookup 110 lockers
-- Left "Locker 110 is already taken!"
-- *Main> lockerLookup 105 lockers
-- Right "QOTSA"

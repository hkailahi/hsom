-- 8.2 Record Syntax --

-- data Person = Person String String Int Float String String deriving (Show)
--
-- -- ex
-- -- *Main> let guy = Person "Buddy" "Fink" 43 184.2 "526-2928" "Chocolate"
-- -- *Main> guy
-- -- Person "Buddy" "Fink" 43 184.2 "526-2928" "Chocolate"
--
-- firstName :: Person -> String
-- firstName (Person firstName _ _ _ _ _) = firstName
--
-- lastName :: Person -> String
-- lastName (Person _ lastName _ _ _ _) = lastName
--
-- age :: Person -> Int
-- age (Person _ _ age _ _ _) = age
--
-- height :: Person -> Float
-- height (Person _ _ _ height _ _) = height
--
-- phoneNumber :: Person -> String
-- phoneNumber (Person _ _ _ _ phoneNumber _) = phoneNumber
--
-- flavor :: Person -> String
-- flavor (Person _ _ _ _ _ flavor) = flavor
--

-- record syntax = alternative easier method for Person
-- data Person = Person {  firstName :: String
--                       , lastName :: String
--                       , age :: Int
--                       , height :: Float
--                       , phoneNumber :: String
--                       , flavor :: String
--                       } deriving (Show)

-- 8.4 Derived Instances --

data Person = Person {  firstName :: String
                      , lastName :: String
                      , age :: Int
                      } deriving (Eq, Show, Read)

-- ex.
-- *Main> let mikeD = Person {firstName = "Michael", lastName = "Diamond", age = 43}
-- *Main> let adRock = Person {firstName = "Adam", lastName = "Horovitz", age = 41}
-- *Main> let mca = Person {firstName = "Adam", lastName = "Yauch", age = 44}
-- *Main> mca == adRock
-- False
-- *Main> mikeD == adRock
-- False
-- *Main> mikeD == mikeD
-- True
-- *Main> mikeD == Person {firstName = "Michael", lastName = "Diamond", age = 43}
-- True
-- *Main> let beastieBoys = [mca, adRock, mikeD]
-- *Main> mikeD `elem` beastieBoys
-- True
-- *Main> "mikeD is: " ++ show mikeD
-- "mikeD is: Person {firstName = \"Michael\", lastName = \"Diamond\", age = 43}"

data Vector a = Vector a a a deriving (Show)

vAdd :: (Num t) => Vector t -> Vector t -> Vector t
(Vector i j k) `vAdd` (Vector l m n) = Vector (i+l) (j+m) (k+n)

dotProd :: (Num t) => Vector t -> Vector t -> t
(Vector i j k) `dotProd` (Vector l m n) = i*l + j*m + k*n

vMult :: (Num t) => Vector t -> t -> Vector t
(Vector i j k) `vMult` m = Vector (i*m) (j*m) (k*m)

-- ex.
-- Vector 3 5 8 `vAdd` Vector 9 2 8
-- Vector 3 5 8 `vAdd` Vector 9 2 8 `vAdd` Vector 0 2 3
-- Vector 3 9 7 `vMult` 10
-- Vector 4 9 5 `dotProd` Vector 9.0 2.0 4.0
-- Vector 2 9 3 `vMult` (Vector 4 9 5 `dotProd` Vector 9 2 4)

-- data NewList a = Empty | Cons a (List a) deriving (Show, Read, Eq, Ord)

-- record syntax
data NewList a = Emptu | Cons { listHead :: a, listTail :: List a} deriving (Show, Read, Eq, Ord)

infixr 5 :-:
data NewList a = Empty | a :-: (List a) deriving (Show, Read, Eq, Ord)

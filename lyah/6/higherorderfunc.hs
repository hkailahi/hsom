multThree :: (Num a) => a -> a -> a -> a
multThree x y z = x * y * z

-- let multTwoWithNine = multThree 9
-- multTwoWithNine 2 3 will equal 54
-- let multWithEighteen = multTwoWithNine 2
-- multWithEighteen 10 will equal 180

compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred x = compare 100 x

compareWithHundred2 :: (Num a, Ord a) => a -> Ordering
compareWithHundred2 = compare 100

divideByTen :: (Floating a) => a -> a
divideByTen = (/10)

isUpperAlphaNum :: Char -> Bool
isUpperAlphaNum = (`elem` ['A'..'Z'])

-- 6.2 Some higher-orderism is in order --

applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = g
  where g x y = f y x

flip2' :: (a -> b -> c) -> b -> a -> c
flip2' f y x = f x y

-- 6.3 Maps and Filters --

map1 :: (a -> b) -> [a] -> [b]
map1 _ [] = []
map1 f (x:xs) = f x : map f xs

filter1 :: (a -> Bool) -> [a] -> [a]
filter1 _ [] = []
filter1 p (x:xs)
  | p x = x : filter1 p xs
  | otherwise = filter1 p xs

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
  let smallerSorted = quicksort (filter (<=x) xs)
      biggerSorted = quicksort (filter (>x) xs)
  in  smallerSorted ++ [x] ++ biggerSorted

-- function to find largest number under 100000 divisible b 3829
largestDivisible :: (Integral a) => a
largestDivisible = head (filter p [100000,99999..])
  where p x = x `mod` 3892 == 0

-- Collatz Sequence: if even (/2) else if odd (*3) + (1)
chain :: (Integral a) => a -> [a]
chain 1 = [1]
chain n
  | even n = n : chain (n `div` 2)
  | odd n = n : chain (n*3 + 1)

-- Answers: For all starting numbers between 1 and 100, hom many
-- Collatz chains have a length greater 15?
numLongChains :: Int
numLongChains = length (filter isLong (map chain [1..100]))
  where isLong xs = length xs > 15

-- Same using Lambdas
numLongChains1 :: Int
numLongChains1 = length (filter (\xs -> length xs > 15) (map chain [1..100]))

-- The following are the same due to the way functions are curried by default
addThree :: (Num a) => a -> a -> a -> a
addThree x y z = x + y + z

addThree1 :: (Num a) => a -> a -> a -> a
addThree1 = \x -> \y -> \z -> x + y + z

flip3' :: (a -> b -> c) -> b -> a -> c
flip3' f = \x y -> f y x

-- 6.5 Only folds and horses --

-- Sum implementation with fold instead of explicit recursion
sum' :: (Num a) => [a] -> a
sum' xs = foldl (\acc x -> acc + x) 0 xs
-- example sum' [3,5,2,1]
-- fold on (\acc x -> acc + x ) start:0 list:xs
-- acc is 0, then acc+x = 0+3 = new acc, then acc+x=3+5, etc until 10+1=11, then acc returned as 11
-- 0 + 3 , [3,5,2,1]
-- 3 + 5 , [5,2,1]
-- 8 + 2 , [2,1]
-- 10 + 1 , [1] = 11

sum1' :: (Num a) => [a] -> a
sum1' = foldl (+) 0
-- (\acc x -> acc + x) and (+) are equivalent
-- if (foo a = bar b a), then that is the same as foo bar b, because of currying

elem' :: (Eq a) => a -> [a] -> Bool
elem' y ys = foldl (\acc x -> if x == y then True else acc) False ys -- x corresponds to (Num a) -> a parameter
-- starting value is False (boolean), list is ys
-- acc is False, then if currY == elem return true, else acc meaning next y in ys

map' :: (a -> b) -> [a] -> [b]
map' f xs = foldr (\x acc -> f x : acc) [] xs
-- right fold
-- acc starts as [], then
-- example map' (+3) [1,2,3]
-- (+3) is f, x has to related to xs as it isn't a function ??
--                      x acc -> f x : acc
-- []                   from starting value
-- 6:[]                 from (+3) 3 :[]
-- 5:6:[] (aka [5,6]    from (+3) 2 :6:[]
-- 4:5:6:[]             from (+3) 1 :5:6:[]

map1' :: (a -> b) -> [a] -> [b]
map1' f xs = foldl (\acc x -> acc ++ [f x]) [] xs

sqrtSums :: Int
sqrtSums = length (takeWhile (<1000) (scanl1 (+) (map sqrt [1..]))) + 1

-- 6.6 Function application with $ --

-- Function application definition ($)
-- ($) :: (a -> b) -> a -> b
-- f $ x = f x
-- makes right associative parentheses

-- sum (filter (<10) (map (*2) [2..10]))
-- can be rewritten as
-- sum $ filter (<10) $ map (*2) [2..10]

-- map ($ 3) [(4+), (10*), (^2), sqrt]
-- produces
-- [7.0,30.0,9.0,1.7320508075688772]
-- aka
-- [(4+)3, (10*)3, (^2)3 ]

-- Function composition definition (.) aka f(g(x))
-- (.) :: (b -> c) -> (a -> b) -> a -> c
-- f . g = \x -> f (g x)

-- map (\x -> negate (abs x)) [5,-3,-6,7,-3,2,-19,24]
-- can be rewritten as
-- map (negate . abs) [5,-3,-6,7,-3,2,-19,24]
-- which returns
-- [-5,-3,-6,-7,-3,-2,-19,-24]

-- map (\xs -> negate (sum (tail xs))) [[1..5],[3..6],[1..7]]
-- can be rewritten as
-- map (negate . sum . tail) [[1..5],[3..6],[1..7]]
-- which returns
-- [-14,-15,-27]


-- sum (replicate 5 (max 6.7 8.9))
-- is equivalent to
-- (sum . replicate 5) (max 6.7 8.9)
-- which can be rewritten as
-- sum . replicate 5 $ max 6.7 8.9

-- replicate 2 (product (map (*3) (zipWith max [1,2] [4,5])))
-- can be rewritten as
-- replicate 2 . product . map (*3) $ zipWith max [1,2] [4,5]
-- which returns
-- [180,180]

lucky :: (Integral a) => a -> String
lucky 7 = "LUCKY NUMBER 7!"
lucky x = "Sorry, you're out of luck, pal!"

sayMe :: (Integral a) => a -> String;
sayMe 1 = "One!"
sayMe 2 = "Two!"
sayMe 3 = "Three!"
sayMe 4 = "Four!"
sayMe 5 = "Five!"
sayMe x = "Not between 1 and 5"

factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1)

-- Alternate addVectors impl
-- addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
-- addVectors a b = (fst a + fst b, snd a + snd b)

addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

head' :: [a] -> a
head' [] = error "Can't call head on an empty list!"
head' (x:_)  = x

tell :: (Show a) => [a] -> String
tell [] = "The list is empty"
tell (x:[]) = "The list has one element: " ++ show x
tell (x:y:[]) = "The list has two elements: " ++ show x ++ " and " ++ show y
tell (x:y:_) = "This list is long. The first two elements are: " ++ show x ++ " and " ++ show y

length' :: (Num b) => [a] -> b
length' [] = 0
length' (_:xs) = 1 + length' xs

sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

-- Use of "as pattern" with all@(x:xs)
fstLetter :: String -> String
fstLetter "" = "Empty String, whoops!"
fstLetter all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]

-- Use of Guards (pretty 'if statements')
bmiTell :: (RealFloat a) => a -> String
bmiTell bmi
  | bmi <= 18.5 = "Underweight"
  | bmi <= 25.0 = "Good job?"
  | bmi <= 30.0 = "Overweight"
  | otherwise = "Tis a silly metric"

max' :: (Ord a) => a -> a -> a
max' a b
  | a > b = a
  | otherwise = b

myCompare :: (Ord a) => a -> a -> Ordering
a `myCompare` b
  | a > b = GT
  | a == b = EQ
  | otherwise = LT

bmiTellWH :: (RealFloat a) => a -> a -> String
bmiTellWH weight height
  | bmi <= 18.5 = "Underweight"
  | bmi <= 25.0 = "Good job?"
  | bmi <= 30.0 = "Overweight"
  | otherwise = "Tis a silly metric"
  where bmi =  weight / height ^ 2


bmiTellWH2 :: (RealFloat a) => a -> a -> String
bmiTellWH2 weight height
  | bmi <= under = "Underweight"
  | bmi <= normal = "Good job?"
  | bmi <= over = "Overweight"
  | otherwise = "Tis a silly metric"
  where bmi =  weight / height ^2
        under = 18.5
        normal = 25.0
        over = 30.0

bmiTellWH3 :: (RealFloat a) => a -> a -> String
bmiTellWH3 weight height
  | bmi <= under = "Underweight"
  | bmi <= normal = "Good job?"
  | bmi <= over = "Overweight"
  | otherwise = "Tis a silly metric"
  where bmi =  weight / height ^2
        (under, normal, over) = (18.5, 25.0, 30.0)

initials :: String -> String -> String
initials firstName lastName = [f] ++ ". " ++ [l] ++ "."
  where (f:_) = firstName
        (l:_) = lastName

calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi w h | (w, h) <- xs]
  where bmi weight height = weight / height ^2

-- 4.4 Let It Be

cylinder :: (RealFloat a) => a -> a -> a
cylinder r h =
  let sideArea = 2 * pi * r * h
      topArea = pi * r ^2
  in  sideArea + 2 * topArea

calcBmis2 :: (RealFloat a) => [(a, a)] -> [a]
calcBmis2 xs = [bmi | (w, h) <- xs, let bmi = w / h ^2]

-- 4.5 Case Expressions

heada' :: [a] -> a
heada' [] = error "No head for empty lists!"
heada' (x:_) = x

headb' :: [a] -> a
headb' xs = case xs of
  [] -> error "No head for empty lists"
  (x:_) -> x

describeList :: [a] -> String
describeList xs = "The list is " ++ case xs of
  [] -> "empty."
  [x] -> "a singleton list."
  xs -> "a longer list."

describeList2 :: [a] -> String
describeList2 xs = "The list is " ++ what xs
  where
    what [] = "empty"
    what [x] = "a singleton list."
    what xs = "a longer list."

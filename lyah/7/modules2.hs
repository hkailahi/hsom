import qualified Data.Map as Map

-- 7.4 Data.map --

phoneBook = [("betty", "555-2938")
            ,("bonnie", "452-2928")
            ,("patsy", "493-2928")
            ,("lucille", "205-2928")
            ,("wendy", "939-8282")
            ,("penny", "853-2492")
            ]

findKey :: (Eq k) => k -> [(k,v)] -> v
findKey key xs = snd . head . filter (\(k,v) -> key == k) $ xs

-- The above case does not handle case where key we're looking for ins't in the association list
-- The below one does

findKey1 :: (Eq k) => k -> [(k,v)] -> Maybe v
findKey1 key [] = Nothing
findKey1 key ((k,v):xs) = if key == k
                            then Just v
                            else findKey1 key xs

-- Same implemented with a fold
findKey2 :: (Eq k) => k -> [(k,v)] -> Maybe v
findKey2 key = foldr (\(k,v) acc -> if key == k
                                    then Just v
                                    else acc)
    Nothing

fromList1 :: (Ord k) => [(k,v)] -> Map.Map k v
fromList1 = foldr (\(k,v) acc -> Map.insert k v acc) Map.empty

# Learning Haskell

Repository of resources and progress towards learning Haskell. Hsom after the book that inspired me to learn the languange -> "Haskell School of Music"

- Exercises, notes, scratch, books, papers, conference talks, lectures, etc..

- Rating is some fuzzy combination of how much I liked a paper and how personally *cough* applicative *cough* I found it

# Books

##### Introductory

- Version is one my version + latest available (since I last checked)

| Title | Location | Author | Year | Progress | Chapters | Pages | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
| Learn You a Haskell for the Greater Good | lyah/ | Miran Lipovaca   | ? | ✅  | 10/14 | 300? | 4 |  |
| Haskell From First Principles | hffp/ | Chris Allen, Julie Moronuki | 2017 | ✅ | 31/31 | 1228 | 10 | 🔁 19 - 31 |
| Real World Haskell | | Bryan O’Sullivan, John Goerzen, and Don Stewart | 2009 | | 28 | 712 | | |
| Beginning Haskell A Project-Based Approach | | Alejandro Serrano Mena | 2014 | | 16 | 408 | | |

##### Music Programming

| Title | Location | Author | Year | Progress | Chapters | Pages | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
| Haskell School of Music | hsom/ | Paul Hudak, Donya Quick | 2014 | ▶️ | 24 | 441 | | |

##### Must Read

| Title | Location | Author | Year | Progress | Chapters | Pages | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
| Typeclassopedia  | | | | ▶️ | | | | |
| Purely Functional Data Structures  | | Chris Okasaki | | | | ▶️ | | |
| Parallel and Concurrent Programming in Haskell  | | | | | | | | |

##### Not Out Yet

| Title | Author | Notes |
| :-----: | :-----: | :-----: |
| Intermediate Haskell | | |
| Haskell Almanac | | |
| Joy of Haskell | | |

##### Reference

| Title | Location | Author | Year | Progress | Chapters | Pages | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
| What I Wish I Knew Learning Haskell | wiwik/ | Stephan Diel | 2017 | ▶️ | | | | |

##### Compilers and PL

| Title | Location | Author | Year | Progress | Chapters | Pages | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
| Write You a Haskell | | | | | | | | |
| [Write You a Scheme, V2](https://wespiser.com/writings/wyas/00_overview.html) | | | 2016 | ▶️ | | | | |
| Types and Programming Languages (TAPL) | | | | | | | | |

##### Maths

| Title | Location | Author | Year | Progress | Chapters | Pages | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
| Homotopy Type Theory | | | | | | | | |
| Category Theory for Computing Scientist | | | | | | | | |

##### Other

| Title | Location | Author | Year | Progress | Chapters | Pages | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
| Pearls of Functional Algorithm Design | | | | | | | | |

# Exercises

| Title | Num of Qs | Progress | Notes |
| :-----: | :-----: | :-----: | :---: | :---: |
| [List](https://wiki.haskell.org/99_questions/1_to_10) | 10 | ✅ | |
| [Lists, continued](https://wiki.haskell.org/99_questions/11_to_20) | 10 | | |
| [Lists again](https://wiki.haskell.org/99_questions/21_to_28) | 8 | | |
| [Arithmetic](https://wiki.haskell.org/99_questions/31_to_41) | 10 | | |
| [Logic and codes](https://wiki.haskell.org/99_questions/46_to_50) | 5 | | |
| [Binary trees](https://wiki.haskell.org/99_questions/54A_to_60) | 7 | | |
| [Binary trees, continued](https://wiki.haskell.org/99_questions/61_to_69) | 9 | | |
| [Multiway trees](https://wiki.haskell.org/99_questions/70B_to_73) | 4 | | |
| [Graphs](https://wiki.haskell.org/99_questions/80_to_89) | 10 | | |
| [Miscellaneous problems](https://wiki.haskell.org/99_questions/90_to_94) | 5 | | |
| [Miscellaneous problems, continued](https://wiki.haskell.org/99_questions/95_to_99) | 5 | | |

# Papers

##### Recommended and Acclaimed

| Title | Author | Year | Progress | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
|  Lambda the Ultimate (Lambda Papers?) | | | | | |
| Fast Mergeable Integer Maps | Chris Okasaki | | ▶️ | |
| Monoids: Theme and Variations | Brent Yorgey | | | | |
| Bidirectionalization for Free! | Janis Voigtlander | | | | |
| Applicative programming with eﬀects | McBride and Patterson | | | |
| Enumerating the Rationals | Gibbons, Lester, and Bird | | | |
| Composing Fractals | Mark Jones | | | |
| i am not a number--i am a free variable | Conor McBride | | | |
| Type-Safe Cast | Stephanie Wierich | | | |
| A Poor Man's Concurrency Monad | Koen Claessen | | | |
| Explaining binomial heaps | Ralf Hinze | | | |
| Power series, power serious | M. Douglas McIlroy | | | |
| Monadic parsing in Haskell | Hutton and Meijer | | ▶️ | |
| The Zipper | Gerard Huet | | | |
| The Third Homomorphism Theorem | Jeremy Gibbons | | | |
| Fun with Semirings | Stephan Dolan | | | |
| Can programming be liberated from the von Neumann style? | | | | |
| The Hindley-Milner Type System | | | | |
| Why Functional Programming Matters | | | | |
| Lazy Functional State Threads | | | | |
| The Essence of the Iterator Pattern | | | | |
| Composable Memory Transactions | | | | |
| Functional Programming with Bananas, Lenses, Envelopes, and Barbed Wire | | | | |
| Sorting and Searching by Distribution: From Generic Discrimination to Generic Tries | Fritz Henglein, Ralf Hinze | | | |
| Trees That Grow | | | | |
| Beautiful Concurrency | | | | |
| Infinite sets that admit fast exhaustive search | | | | |
| Data types a la carte | | | | |
| Notions of Computations as Monoids | | | | |
| A program to solve Sudoku | | | | |
| The countdown problem | | | | |
| Calculating Correct Compilers | | | | |
| OutsideIn(X) - Modular type inference with local assumptions | | | | |
| Typed Tagless Final Interpreters | | | | |
| Supercompilation by Evaluation | | | | |
| Freer Monads, More Extensible Effects | | | | |
| Finger trees: a simple general-purpose data structure | | | | |
| Stream Fusion: From Lists to Streams to Nothing at All | | | | |
| Functional Pearl: Implicit Configurations | | | | |
| Dependently Typed Programming with Singletons | | | | |
| A History of Haskell: Being Lazy With Class | | | | |
| Uniform Boilerplate and List Processing | | | | |
| Typing Haskell in Haskell | | | | |
| Implementing functional languages: a tutorial | | | | |
| There is no Fork: an Abstraction for Efficient, Concurrent, and Concise Data Access | | | | |
| Functional Pearl: Getting a Quick Fix on Comonads | | | | |
| Physics, Topology, Logic and Computation: A Rosetta Stone | | | | |
| A Unified Theory of Garbage Collection | | | | |
| Propositions as Types | | | | |
| Interpreting types as abstract values | | | | |
| Clowns to the Left of me, Jokers to the Right | | | | |
| Burritos for the Hungry Mathematician | | | | |


##### Music Programming

| Title | Author | Year | Progress | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
|  A Functional Approach To Automatic Melody Harmonisation | | | | | |
| Analysis of Musical Structures - An Approach Utilising Monadic Parser Combinators | | | | | |
| Automatic Functional Harmonic Analysis | | | | | |
| Automatic Harmonic Analysis of Jazz Chord Progressions Using a Musical Categorial Grammar | | | | | |
| Autotabber - Optimal guitar tablature with dynamic programming | | | ✅ | | |
| FHarmCompEtc Proposal | | | ✅ | | |
| Functional Generation of Harmony and Melody | | | | | |
| Functional Modelling of Musical Harmony | | | ▶️ | | |
| Harmonic Analysis of Music Using Combinatory Categorial Grammar | | | | | |
| HarmTrace - Improving Similarity Estimation Using Functional Harmony Analysis | | | | | |
| HarmTrace-ISMIR11-final | | | | | |
| Improving Audio Chord Transciption By Exploiting Harmonic and Metric Knowledge | | | | | |
| Statistical Parsing for Harmonic Analysis of Jazz Chord Sequences | | | | | |

# Blogposts

| Title/URL | Author | Year | Progress | Rating | Notes |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: |
| [An Intro to Recursion Schemes](http://blog.sumtypeofway.com/an-introduction-to-recursion-schemes/) | | | | | |
| [Understanding F-Algebras](https://www.schoolofhaskell.com/user/bartosz/understanding-algebras)  | | | | | |
| [A Very General Method of Computing Shortest Paths](http://r6.ca/blog/20110808T035622Z.html) | | | | | Literate Haskell |
|  [Typed final (tagless-final) style](http://okmij.org/ftp/tagless-final/#course-oxford) | | | | | |
|  [The Compromiseless Reconciliation of I/O and Purity](https://blog.jle.im/entry/the-compromiseless-reconciliation-of-i-o-and-purity) | Justin Le | 2013 | ✅ | 10 | 🔁, IO, I like referring to monadic values as actions for accessability |

# Conference, Talks, and Lectures

| Title/URL | Author | Year | Progress | Rating | Notes | Time | Slides |
| :-----: | :-----: | :-----: | :---: | :---: | :---:| :---: | :---: |  :---: |  :---: | :---: |  :---: |
| The Extented Functor Family (https://youtu.be/JZPXzJ5tp9w) | George Wilson | 2016 | ✅ | 10 | 🔁 | |  |
| Escape From the Ivory Towers | | | | | | |
| Category Theory Lecture series | | | | | | |
| http://functionaltalks.org/2014/11/23/runar-oli-bjarnason-free-monad/ | | | | | | |
| https://skillsmatter.com/skillscasts/8261-papers-we-love-meetup | | | | | | |
| https://skillsmatter.com/skillscasts/8261-papers-we-love-meetup | | | | | | |
| [A Categorical View of Computational Effects](https://www.youtube.com/watch?v=6t6bsWVOIzs) | Dr. Emily Riehl | 2017 | | | Lawvere Theory | | http://www.math.jhu.edu/~eriehl/compose.pdf |



# needs sorting:
- https://golem.ph.utexas.edu/category/2017/06/gphenriched_lawvere_theories_f.html
- https://ncatlab.org/nlab/show/Lawvere+theory
- https://www.schoolofhaskell.com/user/edwardk/editorial/procrustean-mathematics -- ed kmett
- https://gilbert.ghost.io/type-systems-for-beginners-an-introduction/
- http://blog.felipe.rs/2017/07/07/where-do-type-systems-come-from/
- https://skillsmatter.com/skillscasts/3932-a-haskell-lecture-with-leading-expert-andres-loh
- http://www.tac.mta.ca/tac/
- https://ncatlab.org/nlab/show/HomePage
- https://github.com/bos/aeson
- https://www.youtube.com/watch?v=Qam6t9EN5SQ
  - https://github.com/conal/talk-2016-generic-fft
- https://ro-che.info/articles/2017-06-17-generic-unification
- https://golem.ph.utexas.edu/category/
- https://github.com/jozefg/learn-tt  (** THIS LOOKS GREAT **)
- https://www.amazon.com/Type-Theory-Formal-Proof-Introduction/dp/110703650X
- "How to prove it" by Velleman
- http://gilmi.xyz/post/2015/02/25/after-lyah
- https://wespiser.com/writings/wyas/00_overview.html (** LOOKS GREAT **) (** PRIORITY **)
- https://www.fpcomplete.com/haskell-syllabus
- https://blog.jle.im/entry/the-compromiseless-reconciliation-of-i-o-and-purity

-- Code from https://blog.jle.im/entry/the-compromiseless-reconciliation-of-i-o-and-purity

import Control.Monad

-- ****************** 1: code from blogpost ****************** --

--  factorial n: n!
factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n-1)

--  fib n: the nth Fibonacci number
fib :: Int -> Int
fib 0 = 1
fib 1 = 1
fib n = fib (n-2) + fib (n-1)

--  first_n_fibs n: a list of the first n Fibonacci numbers
first_n_fibs :: Int -> [Int]
first_n_fibs n = fmap fib [1..n]

--  getStringFromStdin: returns a computation that represents the act of
--      getting a string from stdin.  or rather, a series of instructions on
--      interacting with the computer and generating a String.
getStringFromStdin :: IO String
getStringFromStdin = getLine

--  printFibN: returns a computation that represents the act of printing the
--      nth Fibonacci number to stdout and returns () (Nothing).  or rather,
--      a series of instruction on interacting with the computer to get it to
--      print a Fibonacci number and returning nothing.
printFibN :: Int -> IO ()
printFibN n = print (fib n)

-- getStringAndPrint :: IO ()
-- getStringAndPrint = print (getStringAndPrint $ getStringFromStdin)

-- ****************** 2: some code by me (using above) ****************** --

getStringAndPrint :: IO String
getStringAndPrint = do
  x <- getLine
  return x

-- ^ desugared do-notation
getStringAndPrint' :: IO String
getStringAndPrint' = getLine >>= (\str -> return str)

getIntAndPrintFib :: IO Int
getIntAndPrintFib = do
  x <- getLine
  return $ fib $ read x

-- ^ desugared do-notation
getIntAndPrintFib' :: IO Int
getIntAndPrintFib' = getLine >>= (\i -> return $ fib $ read i)

getIntAndPrintFibList :: IO [Int]
getIntAndPrintFibList = do
  x <- getLine
  return $ first_n_fibs $ read x

-- ^ desugared do-notation
getIntAndPrintFibList' :: IO [Int]
getIntAndPrintFibList' = getLine >>= (\i -> return $ first_n_fibs $ read i)

-- THIS FILE ISN'T MEANT TO COMPILE
-- rather, it shows some underlying constructs of Euterpea

bn, wn, hn, qn, en, sn, tn, sfn, dwn, dhn, dqn, den, dsn, dtn, ddhn, ddqn, dden :: Dur
bnr, wnr, hnr, qnr, enr, snr, tnr, sfnr, dwnr, dhnr, dqnr, denr, dsnr, dtnr, ddhnr, ddqnr, ddenr :: Music Pitch

bn = 2
wn = 1
hn = 1/2
qn = 1/4
en = 1/8
sn = 1/16
tn = 1/32
sfn = 1/64

bnr = rest bn     -- brevis rest
wnr = rest wn     -- whole note rest
hnr = rest hn     -- half note rest
qnr = rest qn     -- quarter note rest
enr = rest en     -- eighth note rest
snr = rest sn     -- sixteenth note rest
tnr = rest tn     -- thirty-second note rest
sfnr = rest sfn   -- sixty-fourth note rest

dwn = 3/2
dhn = 3/4
dqn = 3/8
den = 3/16
dsn = 3/32
dtn = 3/64

dwnr = rest dwn   -- dotted whole note rest
dhnr = rest dhn   -- dotted half note rest
dqnr = rest dqn   -- dotted quarter note rest
denr = rest den   -- dotted eighth note rest
dsnr = rest dsn   -- dotted sixteenth note rest
dtnr = rest dtn   -- dotted thirty-second note rest


ddhn = 7/8
ddqn = 7/16
dden = 7/32

ddhnr = rest ddhn  -- double-dotted half note rest
ddqnr = rest ddqn  -- double-dotted quarter note rest
ddenr = rest dden  -- double-dotted eighth note rest

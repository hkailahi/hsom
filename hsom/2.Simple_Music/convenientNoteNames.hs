-- THIS FILE ISN'T MEANT TO COMPILE
-- rather, it shows some underlying constructs of Euterpea

cff , cf , c, cs, css, dff , df , d, ds, dss, eff , ef , e, es, ess
, fff , ff , f , fs, fss, gff , gf , g, gs, gss, aff , af , a, as
, ass, bff , bf , b, bs, bss :: Octave → Dur → Music Pitch
-- C's
cff o d = note d (Cff , o)
cf o d = note d (Cf , o)
c o d = note d (C, o)
cs o d = note d (Cs, o)
css o d = note d (Css, o)
-- D's
dff o d = note d (Dff , o)
df o d = note d (Df , o)
d o d = note d (D, o)
ds o d = note d (Ds, o)
dss o d = note d (Dss, o)
-- E's
eff o d = note d (Eff , o)
ef o d = note d (Ef , o)
e o d = note d (E, o)
es o d = note d (Es, o)
ess o d = note d (Ess, o)
-- F's
fff o d = note d (Fff , o)
ff o d = note d (Ff , o)
f o d = note d (F, o)
fs o d = note d (Fs, o)
fss o d = note d (Fss, o)
-- G's
gff o d = note d (Gff , o)
gf o d = note d (Gf , o)
g o d = note d (G, o)
gs o d = note d (Gs, o)
gss o d = note d (Gss, o)
-- A's
aff o d = note d (Aff , o)
af o d = note d (Af , o)
a o d = note d (A, o)
as o d = note d (As, o)
ass o d = note d (Ass, o)
-- B's
bff o d = note d (Bff , o)
bf o d = note d (Bf , o)
b o d = note d (B, o)
bs o d = note d (Bs, o)
bss o d = note d (Bss, o)

-- THIS FILE ISN'T MEANT TO COMPILE
-- rather, it shows some underlying constructs of Euterpea


-- Book goes over type synonyms in context of music

type Octave = Int
type Pitch = (PitchClass, Octave)         -- Ex. A440 = (A, 4)
type Dur = Rational                       -- duration -- rational is of form num % denom

qn :: Dur
qn = 1/4 -- quarter note

-- Book defines PitchClass as ADT (algebraic data type)

-- review: PitchClass represented as a sumtype
data PitchClass = Cff | Cf | C | Dff | Cs | Df | Css | D | Eff | Ds
                | Ef | Fff | Dss | E | Ff | Es | F | Gff | Ess | Fs
                | Gf | Fss | G | Aff | Gs | Af | Gss | A | Bff | As
                | Bf | Ass | B | Bs | Bss

data PrimitiveOld = Note Dur Pitch
                  | Rest Dur

-- SomeNoteFunc :: Dur -> Pitch -> PrimitiveOld
-- SomeRestFunc :: Dur          -> PrimitiveOld

-- Redefining Primitive as polymorphic data type
data Primitive a = Note Dur a
                 | Rest Dur
-- type variable 'a' is generalized and could take on more data such as Primitive (Pitch, Loudness)

-- SomeNoteFunc :: Dur -> a -> Primitive a
-- SomeRestFunc :: Dur      -> Primitive a

data Music a =
    Prim (Primitive a)        -- primitive value
  | Music a :+: Music a       -- sequential composition
  | Music a :=: Music a       -- parallel composition
  | Modify Control (Music a)  -- modifier

-- types of constructors above
Prim    :: Primitive a             -> Music a
(:+:)   :: Music a      -> Music a -> Music a
(:=:)   :: Music a      -> Music a -> Music a
Modify  :: Control a    -> Music a -> Music a

data Control =
    Tempo      Rational             -- scale the tempo
  | Transpose  AbsPitch             -- transposition
  | Instrument InstrumentName       -- instrument label
  | Phrase     [PhraseAttribute]    -- phrase attributes
  | Player     PlayerName           -- player label
  | KeySig     PitchClass Mode      -- key signature and mode

type PlayerName = String
data Mode       = Major | Minor

-- Convient Auxiliary Functions
note :: Dur -> a -> Music a
note d p = Prim (Note d p)

rest :: Dur -> Music a
rest d = Prim (Rest d)

tempo :: Dur -> Music a -> Music a
tempo r m = Modify (Tempo r) m

transpose :: AbsPitch -> Music a -> Music a
transpose i m = Modify (Transpose i) m

instrument :: InstrumentName -> Music a -> Music a
instrument i m = Modify (Instrument i) m

phrase :: [PhraseAttribute ] -> Music a -> Music a
phrase pa m = Modify (Phrase pa) m

player :: PlayerName -> Music a -> Music a
player pn m = Modify (Player pn) m

keysig :: PitchClass -> Mode -> Music a -> Music a
keysig pc mo m = Modify (KeySig pc mo) m

data InstrumentName = AcousticGrandPiano | BrightAcousticPiano | ElectricGrandPiano | HonkyTonkPiano | RhodesPiano | ChorusedPiano
                    | Harpsichord | Clavinet | Celesta | Glockenspiel | MusicBox | Vibraphone | Marimba | Xylophone | TubularBells
                    | Dulcimer | HammondOrgan | PercussiveOrgan | RockOrgan | ChurchOrgan | ReedOrgan | Accordion | Harmonica
                    | TangoAccordion | AcousticGuitarNylon | AcousticGuitarSteel | ElectricGuitarJazz | ElectricGuitarClean
                    | ElectricGuitarMuted | OverdrivenGuitar | DistortionGuitar | GuitarHarmonics | AcousticBass | ElectricBassFingered
                    | ElectricBassPicked | FretlessBass | SlapBass1 | SlapBass2 | SynthBass1 | SynthBass2 | Violin | Viola | Cello
                    | Contrabass | TremoloStrings | PizzicatoStrings | OrchestralHarp | Timpani | StringEnsemble1 | StringEnsemble2
                    | SynthStrings1 | SynthStrings2 | ChoirAahs | VoiceOohs | SynthVoice | OrchestraHit | Trumpet | Trombone | Tuba
                    | MutedTrumpet | FrenchHorn | BrassSection | SynthBrass1 | SynthBrass2 | SopranoSax | AltoSax | TenorSax
                    | BaritoneSax | Oboe | Bassoon | EnglishHorn | Clarinet | Piccolo | Flute | Recorder | PanFlute | BlownBottle
                    | Shakuhachi | Whistle | Ocarina | Lead1Square | Lead2Sawtooth | Lead3Calliope | Lead4Chiﬀ | Lead5Charang | Lead6Voice
                    | Lead7Fifths | Lead8BassLead | Pad1NewAge | Pad2Warm | Pad3Polysynth | Pad4Choir | Pad5Bowed | Pad6Metallic | Pad7Halo
                    | Pad8Sweep | FX1Train | FX2Soundtrack | FX3Crystal | FX4Atmosphere | FX5Brightness | FX6Goblins | FX7Echoes | FX8SciFi
                    | Sitar | Banjo | Shamisen | Koto | Kalimba | Bagpipe | Fiddle | Shanai | TinkleBell | Agogo | SteelDrums | Woodblock
                    | TaikoDrum | MelodicDrum | SynthDrum | ReverseCymbal | GuitarFretNoise | BreathNoise | Seashore | BirdTweet
                    | TelephoneRing | Helicopter | Applause | Gunshot | Percussion | Custom String

import Euterpea
import Control.Applicative

mkMajorChord :: Pitch -> Music Pitch
mkMajorChord p = fmap (note wn) (p :=: (trans 4 p) :=: (trans 7 p))

mkMinorChord :: Pitch -> Music Pitch
mkMinorChord p = fmap (note wn) (p :=: (trans 3 p) :=: (trans 7 p))



twoFiveOne :: Pitch -> Dur -> Music Pitch
twoFiveOne p d = let two = note d p :=: note d (trans 3 p) :=: note d (trans 7 p)
                     five = note d (trans 5 p) :=: note d (trans 9 p) :=: note d (trans 12 p)
                     one =  note (d*2) (trans (-2) p) :=: note (d*2) (trans 2 p) :=: note (d*2) ( trans 5 p)
                 in two :+: five :+: one

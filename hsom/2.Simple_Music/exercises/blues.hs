-- Exercise 2.2

import Euterpea

-- Blues Pentatonic
data BluesPitchClass = Ro   -- root
                     | MT   -- minor third
                     | Fo   -- fourth
                     | Fi   -- fifth
                     | MS   -- minor seventh


type BluesPitch = (BluesPitchClass, Octave)

ro, mt, fo, fi, ms :: Octave -> Dur -> Music BluesPitch
ro o d = note d (Ro, o);
mt o d = note d (MT, o);
fo o d = note d (Fo, o);
fi o d = note d (Fi, o);
ms o d = note d (MS, o);


fromBlues :: Music BluesPitch -> Music Pitch
fromBlues (Prim (Rest d)) = Prim (Rest d)
fromBlues (Prim (Note d (Ro, o))) = Prim (Note d (C, o))
fromBlues (Prim (Note d (MT, o))) = Prim (Note d (Ef, o))
fromBlues (Prim (Note d (Fo, o))) = Prim (Note d (F, o))
fromBlues (Prim (Note d (Fi, o))) = Prim (Note d (G, o))
fromBlues (Prim (Note d (MS, o))) = Prim (Note d (Bf, o))
fromBlues (m1 :+: m2) = fromBlues(m1) :+: fromBlues(m2)
fromBlues (m1 :=: m2) = fromBlues(m1) :=: fromBlues(m2)




melody1 = fromBlues( fi 4 dqn :+: ms 4 en :+: fo 4 qn :+: mt 4 hn :+: ro 4 qn :+: mt 4 hn )
melody2 = fromBlues( fi 3 dhn :+: mt 3 dhn :+: fi 3 dhn :+: rest hn )
melody3 = fromBlues( fi 5 qn :+: rest en :+: times 2 ( fi 5 sn ) :+:
                     fi 5 en :+: rest sn :+: fi 5 en :+: rest sn :+:
                     times 2 (fi 5 qn ) :+: fi 5 en )
melody4 = fromBlues( mt 6 qn :+: rest en :+: times 2 ( mt 6 sn ) :+:
                     mt 5 en :+: rest sn :+: mt 6 en :+: rest sn :+:
                     times 2 (mt 6 qn ) :+: mt 6 en )

bluesSong = times 2 ( melody1 :=: melody2 :=: melody3 :=: melody4)

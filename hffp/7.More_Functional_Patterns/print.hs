-- putStr :: String -> IO ()
-- putStrLn :: String -> IO ()
-- print :: Show a => a -> IO ()
-- show :: Show a -> a -> String
-- (.) :: (b -> c) -> (a -> b) -> a -> c

-- print is actually a composition of putStrLn and Show
-- so ...
print1 :: Show a -> a -> IO ()
print1 a = putStrLn (show a)

-- ...is equivalent to
print2 :: Show a -> a -> IO ()
print2 a = (putStrLn . show) a -- aka f(g(x)) aka putStrLn (show (a))

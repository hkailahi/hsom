myNum :: Integer
myNum = 1

myVal f = myNum

  -- ex. myNum output
-- *Main> :t myVal
-- myVal :: t -> Integer
-- *Main> let myNum = 1 :: Integer
-- *Main> let myVal f = f + myNum
-- *Main> :t myVal
-- myVal :: Integer -> Integer
-- *Main> let myNum = 1 :: Float
-- *Main> let myVal f = f + myNum
-- *Main> :t myVal
-- myVal :: Float -> Float

-- *Main> let myNum = 1 :: Integer
-- *Main> let myVal f = f + myNum
-- *Main> :t myVal
-- myVal :: Integer -> Integer
-- *Main> let myVal f g = myNum
-- *Main> :t myVal
-- myVal :: t1 -> t -> Integer
-- *Main> :t myVal
-- myVal :: t2 -> t1 -> t -> Integer

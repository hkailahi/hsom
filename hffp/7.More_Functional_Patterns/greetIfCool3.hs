module GreetIfCool3 where
  greetIfCool :: String -> IO ()
  greetIfCool coolness =
    case cool of
      True -> putStrLn "eyyyy. Looking what is, cooking good"
      False ->putStrLn "nah gtfo m'face fam"
    where cool = coolness == "downright frosty yo"

--  vs alternative if-then-else case
-- module GreetIfCool2 where
--   greetIfCool :: String -> IO ()
--   greetIfCool coolness =
--     if cool coolness
--       then putStrLn "eyyyy. Looking what is, cooking good"
--     else
--       putStrLn "nah gtfo m'face fam"
--     where cool v = v == "downright frosty yo"

-- or other alternative
-- module GreetIfCool1 where
--   greetIfCool :: String -> IO ()
--   greetIfCool coolness =
--     if cool
--       then putStrLn "eyyyy. Looking what is, cooking good"
--     else
--       putStrLn "nah gtfo m'face fam"
--     where cool = coolness == "downright frosty yo"

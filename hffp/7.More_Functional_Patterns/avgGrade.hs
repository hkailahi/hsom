avgGrade :: (Fractional a, Ord a) => a -> Char
avgGrade x
  | y >= 0.9  = 'A'
  | y >= 0.8  = 'B'
  | y >= 0.7  = 'C'
  | y >= 0.59 = 'D'
  | y < 0.59  = 'F'
  where y = x / 100

  -- ex. avgGrade output
-- *Main> avgGrade 90.12314234234
-- 'A'
-- *Main> avgGrade $ 20 * 4
-- 'B'
-- *Main> avgGrade 1
-- 'F'

data Employee = Coder
              | Manager
              | Veep
              | CEO
              deriving (Eq, Ord, Show)

reportBoss :: Employee -> Employee -> IO ()
reportBoss e e' =
  putStrLn $ show e ++ " is the boss of " ++ show e'

codersRuleCEOsDrool :: Employee -> Employee -> Ordering
codersRuleCEOsDrool Coder Coder = EQ
codersRuleCEOsDrool Coder _     = GT
codersRuleCEOsDrool _ Coder     = LT
codersRuleCEOsDrool e e'        = compare e e'

employeeRank :: Employee -> Employee -> IO ()
employeeRank e e' =
  case compare e e' of
    GT -> reportBoss e e'
    EQ -> putStrLn "Neither employee is the boss"
    LT -> (flip reportBoss) e e'

employeeRank2 :: (Employee -> Employee -> Ordering)
                -> Employee
                -> Employee
                -> IO ()
employeeRank2 f e e' =
  case f e e' of
    GT -> reportBoss e e'
    EQ -> putStrLn "Neither employee is the boss"
    LT -> (flip reportBoss) e e'

  --  ex. employee output
-- *Main> employeeRank Coder Coder
-- Neither employee is the boss
-- *Main> employeeRank Coder CEO
-- CEO is the boss of Coder
-- *Main> employeeRank Coder CEO
-- CEO is the boss of Coder
-- *Main> employeeRank2 compare Veep CEO
-- CEO is the boss of Veep

-- *Main> employeeRank2 codersRuleCEOsDrool CEO Coder
-- Coder is the boss of CEO
-- *Main> employeeRank2 codersRuleCEOsDrool Coder Veep
-- Coder is the boss of Veep
-- *Main> employeeRank2 codersRuleCEOsDrool Coder Coder
-- Neither employee is the boss

module TupleFunctions where

  addEmUp2 :: Num a => (a, a) -> a
  addEmUp2 (x, y) = x + y

  addEmUp2Alt :: Num a => (a, a) -> a
  addEmUp2Alt tup = (fst tup) + (snd tup)

  fst3 :: (a, b, c) -> a
  fst3 (x, _, _) = x

  third3 :: (a, b, c) -> c
  third3 (_, _, x) = x

  -- ex. matchingTuples output
-- TupleFunctions> :browse TupleFunctions
-- addEmUp2 :: Num a => (a, a) -> a
-- addEmUp2Alt :: Num a => (a, a) -> a
-- fst3 :: (a, b, c) -> a
-- third3 :: (a, b, c) -> c

-- *TupleFunctions> addEmUp2 (10,20)
-- 30
-- *TupleFunctions> addEmUp2Alt (10,20)
-- 30
-- *TupleFunctions> fst3 ("blah", 2, [])
-- "blah"
-- *TupleFunctions> third3 ("blah", 2, [])
-- []

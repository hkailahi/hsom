data WherePenguinLive =
    Galapagos
  | Antartic
  | Australia
  | SouthAfrica
  | SouthAmerica
  deriving (Eq, Show)

data Penguin =
  Peng WherePenguinLive
  deriving (Eq, Show)

isSouthAfrica :: WherePenguinLive -> Bool
isSouthAfrica SouthAfrica = True
isSouthAfrica Galapagos = False
isSouthAfrica Antartic = False
isSouthAfrica Australia = False
isSouthAfrica SouthAmerica = False

isSouthAfrica2 :: WherePenguinLive -> Bool
isSouthAfrica2 SouthAfrica = True
isSouthAfrica2 _ = False

gimmeWhereTheyLive :: Penguin -> WherePenguinLive
gimmeWhereTheyLive (Peng whereitlives) = whereitlives

humbbolt = Peng SouthAmerica
gentoo = Peng Antartic
macoroni = Peng Antartic
little = Peng Australia
galapagos = Peng Galapagos

galapagosPenguin :: Penguin -> Bool
galapagosPenguin (Peng Galapagos) = True
galapagosPenguin _                = False

antarticPenguin :: Penguin -> Bool
antarticPenguin (Peng Antartic) = True
antarticPenguin _               = False

antarticOrGalapagos :: Penguin -> Bool
antarticOrGalapagos p =
  (galapagosPenguin p) || (antarticPenguin p)

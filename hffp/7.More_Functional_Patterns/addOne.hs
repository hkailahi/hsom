addOne :: Integer -> Integer
addOne x = x + 1

bindExp :: Integer -> String
bindExp x = let y = 5 in
              "the integer was: " ++ show x
              ++ " and y was: " ++ show y

-- bindExp2 gives error: Variable not in scope: y :: Integer
-- bindExp2 :: Integer -> String
-- bindExp2 x = let z = y + x in
--             let y = 5 in "the integer was: "
--             ++ show x ++ " and y was: "
--             ++ show y ++ " and z was: " ++ show z

bindExp3 :: Integer -> String
bindExp3 x = let x = 10; y = 5 in
              "the integer was: " ++ show x
              ++ " and y was: " ++ show y

module RegisteredUser where

  newtype Username = Username String
  newtype AccountNumber = AccountNumber Integer

  data User = UnregisteredUser | RegisteredUser Username AccountNumber

  printUser :: User -> IO ()
  printUser UnregisteredUser = putStrLn "UnregisteredUser"
  printUser (RegisteredUser (Username name)
                            (AccountNumber acctNum))
            = putStrLn $ name ++ " " ++ show acctNum

  -- ex. registeredUser2 output
-- *RegisteredUser> :t RegisteredUser
-- RegisteredUser :: Username -> AccountNumber -> User
-- *RegisteredUser> :t User
-- User      Username
-- *RegisteredUser> :t Username
-- Username :: String -> Username
-- *RegisteredUser> :t AccountNumber
-- AccountNumber :: Integer -> AccountNumber

-- *RegisteredUser> printUser UnregisteredUser
-- UnregisteredUser
-- *RegisteredUser> let myUser = (Username "callen")
-- *RegisteredUser> let myAcct = (AccountNumber 10456)
-- *RegisteredUser> printUser $ RegisteredUser myUser myAcct
-- callen 10456

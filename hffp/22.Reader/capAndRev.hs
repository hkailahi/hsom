import Data.Char
import Control.Applicative
import Control.Monad

cap :: [Char] -> [Char]
cap xs = map toUpper xs

rev :: [Char] -> [Char]
rev xs = reverse xs

composed :: [Char] -> [Char]
composed = cap . rev

fmapped :: [Char] -> [Char]
fmapped = rev . cap

-- λ: composed "Julie"
-- "EILUJ"
-- λ: fmapped "Chris"
-- "SIRHC"
-- λ: fmapped "Chris" == composed "Chris"
-- True

tupled :: [Char] -> ([Char],[Char])
tupled xs = (cap xs, rev xs)

tupled' :: [Char] -> ([Char],[Char])
tupled' xs = (rev xs, cap xs)

tupledA :: [Char] -> ([Char],[Char])
tupledA = (,) <$> cap <*> rev

tupledA' :: [Char] -> ([Char],[Char])
tupledA' = (,) <$> rev <*> cap

tupledAwithLift :: [Char] -> ([Char],[Char])
tupledAwithLift = liftA2 (,) cap rev

tupledM_do :: [Char] -> ([Char], [Char])
tupledM_do = do
  a <- rev
  b <- cap
  return (a, b)

tupledM_bind :: [Char] -> ([Char], [Char])
tupledM_bind = rev >>= \x1 -> cap >>= \x2 -> return (x1, x2)

-- λ: tupledAwithLift "Julie"
-- ("JULIE","eiluJ")
-- λ: tupledM_bind  "Julie"
-- ("eiluJ","JULIE")
-- λ: tupledM_do "Julie"
-- ("eiluJ","JULIE")

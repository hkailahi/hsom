module EitherMonad where

-- -- m ~ Either e
--
-- (>>=) :: Monad m => m a -> (a -> m b) -> m b (>>=) :: Either e a -> (a -> Either e b) -> Either e b
--
-- -- same as pure
--
-- return :: Monad m => a -> m a return :: a -> Either e a

type Founded = Int -- years ago
type Coders = Int -- Num of programmers

data SoftwareShop =
  Shop {
      found       :: Founded
    , programmers :: Coders
} deriving (Eq, Show)

data FoundedError =
    NegativeYears Founded
  | TooManyYears Founded
  | NegativeCoders Coders
  | TooManyCoders Coders
  | TooManyCodersForYears Founded Coders
  deriving (Eq, Show)

validateFounded :: Int -> Either FoundedError Founded
validateFounded n
  | n < 0       = Left $ NegativeYears n
  | n > 500     = Left $ TooManyYears n
  | otherwise   = Right n

validateCoders :: Int -> Either FoundedError Coders
validateCoders n
  | n < 0       = Left $ NegativeCoders n
  | n > 5000    = Left $ TooManyCoders n
  | otherwise   = Right n

mkSoftware :: Int -> Int -> Either FoundedError SoftwareShop
mkSoftware years coders = do
  founded <- validateFounded years
  programmers <- validateCoders coders
  if programmers > div founded 10
    then Left $ TooManyCodersForYears founded programmers
    else Right $ Shop founded programmers

  -- ex. EitherMonad output
-- λ: mkSoftware 0 0
-- Right (Shop {found = 0, programmers = 0})
-- λ: mkSoftware (-1) 0
-- Left (NegativeYears (-1))
-- λ: mkSoftware (-1) (-1)
-- Left (NegativeYears (-1))
  -- ^ notice it doesn't do NegativeCoders, as Either Monad short-circuits on first fault
  -- !!!! We would have to use Applicatives if we wanted accumulation of errors !!!!
-- λ: mkSoftware 0 (-1)
-- Left (NegativeCoders (-1))
-- λ: mkSoftware 500 0
-- Right (Shop {found = 500, programmers = 0})
-- λ: mkSoftware 501 0
-- Left (TooManyYears 501)
-- λ: mkSoftware 501 501
-- Left (TooManyYears 501)
-- λ: mkSoftware 101 5001
-- Left (TooManyCoders 5001)
-- λ: mkSoftware 0 500
-- Left (TooManyCodersForYears 0 500)

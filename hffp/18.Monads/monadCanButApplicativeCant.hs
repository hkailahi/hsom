f :: Integer -> Maybe Integer
f 0 = Nothing
f n = Just n

g  :: Integer -> Maybe Integer
g i =
  if even i
  then Just (i + 1)
  else Nothing

h :: Integer -> Maybe String
h i = Just ("10191" ++ show i)

-- This can be rewritten with Applicative
dosomethingA = do
  a <- f
  b <- g
  c <- h
  pure (a, b, c)

-- This can't be rewritten with Applicative
dosomethingM n = do
  a <- f n
  b <- g a
  c <- h b
  pure (a, b, c)

  -- ex. dosomethingA and dosomethingM output
-- λ: dosomethingA 1
-- (Just 1,Nothing,Just "101911")
-- λ: dosomethingM 1
-- Nothing

-- λ: dosomethingA 2
-- (Just 2,Just 3,Just "101912")  -- a b and c are independent
-- λ: dosomethingM 2           -- a = f n = (Just) (2) = 2 then b = g a = (+1) (2) = 3 then c = h b = "10191" ++ show (3) = "101913"
-- Just (2,3,"101913")         -- b depends on a, c depends on b and thus also a

-- λ: dosomethingA 3
-- (Just 3,Nothing,Just "101913")
-- λ: dosomethingM 3
-- Nothing

-- λ: dosomethingA 4
-- (Just 4,Just 5,Just "101914")
-- λ: dosomethingM 4
-- Just (4,5,"101915")

-- λ: dosomethingA 5
-- (Just 5,Nothing,Just "101915")
-- λ: dosomethingM 5
-- Nothing

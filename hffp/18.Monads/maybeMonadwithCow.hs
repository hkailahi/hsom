data Cow = Cow {
      name    :: String
    , age     :: Int
    , weight  :: Int
} deriving (Eq, Show)

noEmpty :: String -> Maybe String
noEmpty "" = Nothing
noEmpty str = Just str

noNegative :: Int -> Maybe Int
noNegative n
  | n >= 0 = Just n
  | otherwise = Nothing

-- if Cow's name is Bess, must be under 500
weightCheck :: Cow -> Maybe Cow
weightCheck c =
  let w = weight c
      n = name c
  in if n == "Bess" && w > 499
    then Nothing
    else Just c

mkSphericalCow :: String -> Int -> Int -> Maybe Cow
mkSphericalCow name' age' weight' =
  case noEmpty name' of
    Nothing -> Nothing
    Just nammy ->
      case noNegative age' of
        Nothing -> Nothing
        Just agey ->
          case noNegative weight' of
            Nothing -> Nothing
            Just weighty ->
              weightCheck (Cow nammy agey weighty)

  -- ex. mkSphericalCow output
-- λ: mkSphericalCow "Bess" 5 499
-- Just (Cow {name = "Bess", age = 5, weight = 499})

-- Do syntax isn't just for IO
mkSphericalCow' :: String -> Int -> Int -> Maybe Cow
mkSphericalCow'  name' age' weight' = do
  nammy <- noEmpty name'
  agey <- noNegative age'
  weighty <- noNegative weight'
  weightCheck (Cow nammy agey weighty)

  -- ex. mkSphericalCow' output
-- λ: mkSphericalCow "Bess" 5 499
-- Just (Cow {name = "Bess", age = 5, weight = 499})
-- λ: mkSphericalCow "Bess" 5 500
-- Nothing

-- Writing it with >>=
mkSphericalCow'' :: String -> Int -> Int -> Maybe Cow
mkSphericalCow''  name' age' weight' =
  noEmpty name' >>=
    \ nammy ->
      noNegative age' >>=
      \ agey ->
        noNegative weight' >>=
        \ weighty ->
        weightCheck (Cow nammy agey weighty)

  -- ex. mkSphericalCow'' output
-- λ: mkSphericalCow'' "Bess" 5 500
-- Nothing
-- λ: mkSphericalCow'' "Bess" 5 499
-- Just (Cow {name = "Bess", age = 5, weight = 499})

  -- ex. some playing with >>= in Prelude                   --   m     a   >>=   (a   ->  m     b)   =  m   b
-- λ: Just 5 >>= (\x -> Just (x+5))                         -- Just    5   >>=   (\5  -> Just (5+5)) = Just 10
-- Just 10
-- λ: noNegative 5 >>= (\x -> Just (x+5))       -- (>0 :: Maybe Int)   5   >>=   (\5 -> (5>0) (5+5))= Just 10
-- Just 10
-- λ: Nothing >>= (\x -> Just (x+5))                        -- Nothing ?   >>=   (\? ->  Just (?+5))  = Nothing
-- Nothing
-- λ: Just "hello" >>= (\x -> Just (x ++ "hello"))       -- Just "hello"   >>= (\"hello" -> Just ("hello"++"hello") = Just "hellohello"
-- Just "hellohello"
-- λ: Just "hello" >>= (\x -> Just (x ++ " world!"))
-- Just "hello world!"

-- λ: Nothing >>= undefined
-- Nothing
-- λ: Just 1 >>= undefined
-- *** Exception: Prelude.undefined
-- CallStack (from HasCallStack):
--   error, called at libraries/base/GHC/Err.hs:79:14 in base:GHC.Err
--   undefined, called at <interactive>:170:12 in interactive:Ghci10

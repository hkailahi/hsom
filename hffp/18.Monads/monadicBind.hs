import Control.Monad (join)

-- Exercise: Implement bind in terms of fmap and join

-- keep in mind that this is (>>=) flipped
bind' :: Monad m => (a -> m b) -> m a -> m b
bind' a2M monA = join $ fmap a2M monA
-- or bind' f x = join $ fmap f x

  -- ex. bind' output
-- λ: bind' (\ x -> [x,1]) [4,5,6]
-- [4,1,5,1,6,1]
-- λ:

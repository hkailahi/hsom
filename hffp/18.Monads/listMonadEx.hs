import Control.Monad

twiceWhenEven :: [Integer] -> [Integer]
twiceWhenEven xs = do
  x <- xs
  if even x
    then [x*x, x*x]
    else [x*x]

  -- ex. twiceWhenEven output
-- λ: twiceWhenEven [1..10]
-- [1,4,4,9,16,16,25,36,36,49,64,64,81,100,100]

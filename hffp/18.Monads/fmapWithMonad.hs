import Control.Monad

fmap1 :: Functor f => (a -> b) -> f a -> f b
fmap1 f xs = fmap f xs

-- fmap2 :: Functor f => (a -> b) -> f a -> f b
fmap2 f xs = xs >>= return . f

-- fmap1 is the same as fmap2, showing we can derive functor with monad

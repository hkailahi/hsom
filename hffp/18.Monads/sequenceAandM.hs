import Control.Applicative ((*>))

sequencing :: IO ()
sequencing = do
  putStrLn "blah"
  putStrLn "another thing"

sequencing' :: IO ()
sequencing' =
  putStrLn "blah" >>
  putStrLn "another thing"

sequencing'' :: IO ()
sequencing'' =
  putStrLn "blah" *>
  putStrLn "another thing"

  -- ex. sequencing output
-- λ: sequencing
-- blah
-- another thing
-- λ: sequencing'
-- blah
-- another thing
-- λ: sequencing''
-- blah
-- another thing

binding :: IO ()
binding = do
  putStrLn "Enter name:"
  name <- getLine
  putStrLn name

binding' :: IO ()
binding' =
  getLine >>= putStrLn -- TODO figure out how would we have putStrLn "Enter name:" precede >>=

  -- ex. binding output
  -- "> " refers to me inputting something
-- λ: binding
-- Enter name:
-- > thing
-- thing
-- λ: binding'
-- > hdf
-- hdf

bindingAndSequencing :: IO ()
bindingAndSequencing = do
  putStrLn "Enter name:"
  name <- getLine
  putStrLn ("Hello " ++ name ++ "!")

bindingAndSequencing' :: IO ()
bindingAndSequencing' =
  putStrLn "Enter name:" >>
  getLine >>=
  \name -> putStrLn ("Hello " ++ name ++ "!")

  --
-- λ: bindingAndSequencing
-- Enter name:
-- > Heneli
-- Hello Heneli!
-- λ: bindingAndSequencing'
-- Enter name:
-- > Heneli
-- Hello Heneli!

twoBinds :: IO ()
twoBinds = do
  putStrLn "Enter name:"
  name <- getLine
  putStrLn "Enter age:"
  age <- getLine
  putStrLn ("Hello " ++ name ++
            "! You are " ++ age
            ++ " years old!")

twoBinds' :: IO ()
twoBinds' =
  putStrLn "Enter name:" >>
  getLine >>=
  \name ->
  putStrLn "Enter age:" >>
  getLine >>=
  \age ->
  putStrLn ("Hello " ++ name ++
            "! You are " ++ age
            ++ " years old!")

-- λ: twoBinds
-- Enter name:
-- > Heneli
-- Enter age:
-- > 24
-- λ: twoBinds'
-- Enter name:
-- > Heneli
-- Enter age:
-- > 24
-- Hello Heneli! You are 24 years old!

module Main where

import System.IO

import Hello

-- main :: IO ()
-- main = do
--   name <- getLine
--   sayHello name

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  putStr "Please input your name: "
  name <- getLine
  sayHello name

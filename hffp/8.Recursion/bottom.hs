f :: Bool -> Int
f True = error "blah"
f False = 0

  -- ex. bottom output
-- *Main> f False
-- 0
-- *Main> f True
-- *** Exception: blah
-- CallStack (from HasCallStack):
--   error, called at bottom.hs:2:10 in main:Main

-- returns same as f, except error is " *** Exception: bottom.hs(...) Non-exhaustive patterns in function f' "
f' :: Bool -> Int
f' False = 0

f1 :: Bool -> Int
f1 False = 0
f1 _ = error $ "*** Exception: "
              ++ "Non-exhaustive "
              ++ "patterns in function f"

-- broken, book says it will explain later??
-- data Maybe a = Nothing | Just a
--
-- f2 :: Bool -> Maybe Int
-- f2 False = Just 0 :: Int
-- f2 _ = Nothing

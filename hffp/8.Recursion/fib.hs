-- (fibonnacci :: Integer -> Integer) is equivalent
fibonnacci :: Integral a => a -> a
fibonnacci 0 = 0
fibonnacci 1 = 1
fibonnacci x = fibonnacci (x - 1) + fibonnacci (x - 2)

-- ex. fib output
-- *Main> fibonnacci 10
-- 55
-- *Main> fibonnacci 20
-- 6765
-- *Main> fibonnacci 30
-- 832040 -- this one took a while

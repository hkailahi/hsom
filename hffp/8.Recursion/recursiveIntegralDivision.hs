dividedBy :: Integer -> Integer -> Integer
dividedBy = div -- notice pointfree style

type Numerator = Integer
type Denominator = Integer
type Quotient = Integer

-- equivalent via type synonym
dividedBy' :: Numerator -> Denominator -> Quotient
dividedBy' = div

-- return (quotient, remainder) tuple
dividedBy2 :: Integral a => a -> a -> (a, a)
dividedBy2 num denom = go num denom 0
  where go n d count
          | n < d     = (count, n)
          | otherwise = go (n - d) d (count + 1)

  -- ex. recursiveIntegralDivision output
-- *Main> dividedBy 100 10
-- 10
-- *Main> dividedBy' 100 10
-- 10
-- *Main> 100 `dividedBy'` 10
-- 10

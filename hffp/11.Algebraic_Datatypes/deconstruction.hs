newtype Name = Name String deriving Show
newtype Acres = Acres Int deriving Show

---- The following shows a method of unpacking or deconstructing the contents of the Farmer/FarmerRec product type

-- FarmerType is a Sum
data FarmerType = DairyFarmer | WheatFarmer | SoybeanFarmer deriving Show

-- Farmer is a Product of Name, Acres, and FarmerType
data Farmer = Farmer Name Acres FarmerType deriving Show

isDairyFarmer :: Farmer -> Bool
isDairyFarmer (Farmer _ _ DairyFarmer) = True
isDairyFarmer _ = False

-- just Farmer with record syntax
-- constructor and isDairyFarmerRec are the same thing as above
data FarmerRec =
  FarmerRec { name        :: Name
            , acres       :: Acres
            , farmerType  :: FarmerType } deriving Show

isDairyFarmerRec :: FarmerRec -> Bool
isDairyFarmerRec farmer = case farmerType farmer of
  DairyFarmer -> True
  _           -> False


  -- ex. deconstruction output
-- *Main> let john = Farmer "John" 12 DairyFarmer
--
-- <interactive>:3:19: error:
--     • Couldn't match expected type ‘Name’ with actual type ‘[Char]’
--     • In the first argument of ‘Farmer’, namely ‘"John"’
--       In the expression: Farmer "John" 12 DairyFarmer
--       In an equation for ‘john’: john = Farmer "John" 12 DairyFarmer
-- *Main> let nm = Name "John"
-- *Main> let tw = Acres 12
-- *Main> let john = Farmer nm tw DairyFarmer
-- *Main> isD
-- isDairyFarmer     isDairyFarmerRec  isDenormalized
-- *Main> isDairyFarmer john
-- True
-- *Main>

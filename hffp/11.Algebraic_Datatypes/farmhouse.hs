-- READ THIS, explains newtype vs data -> http://stackoverflow.com/questions/5889696/difference-between-data-and-newtype-in-haskell

data GuessWhat = Chickenbutt deriving (Eq, Show)

data Id a = MkId a deriving (Eq, Show)

data Product a b = Product a b deriving (Eq, Show)

data Sum a b =
    First a
  | Second b
  deriving (Eq, Show)

data RecordProduct a b =
  RecordProduct { pfirst :: a
                , psecond :: b }
                deriving (Eq, Show)

---------------------------------------------------------------------

newtype NumCow =
  NumCow Int
  deriving (Eq, Show)

newtype NumPig =
  NumPig Int
  deriving (Eq, Show)

data Farmhouse =
  Farmhouse NumCow NumPig
  deriving (Eq, Show)

type Farmhouse' = Product NumCow NumPig

-- Farmhouse and Farmhouse' are the same

newtype NumSheep =
  NumSheep Int
  deriving (Eq, Show)

data BigFarmhouse =
  BigFarmhouse NumCow NumPig NumSheep
  deriving (Eq, Show)

type BigFarmhouse' =
  Product NumCow (Product NumPig NumSheep)

type Name = String
type Age = Int
type LovesMud = Bool

type PoundsOfWool = Int

data CowInfo =
  CowInfo Name Age
  deriving (Eq, Show)

data PigInfo =
  PigInfo Name Age
  deriving (Eq, Show)

data SheepInfo =
  SheepInfo Name Age PoundsOfWool
  deriving (Eq, Show)

data Animal =
    Cow CowInfo
  | Pig PigInfo
  | Sheep SheepInfo
  deriving (Eq, Show)

-- alternately

type Animal' =
  Sum CowInfo (Sum PigInfo SheepInfo)

  -- ex. Farmhouse output
  -- "First" and "Second" are used to pattern match on data constructors
-- *Main> let bess = First (CowInfo "Bess" 4) :: Animal'
-- *Main> let elmer' = Second (SheepInfo "Elmer" 5 5)
-- *Main> let elmer = Second elmer' :: Animal'
-- *Main> -- making mistake
-- *Main> let elmo' = Second (SheepInfo "Elmo" 5 5)
-- *Main> let elmo = First elmo' :: Animal'
--
-- <interactive>:30:12: error:
--     • Couldn't match type ‘Sum a0 SheepInfo’ with ‘CowInfo’
--       Expected type: Animal'
--         Actual type: Sum (Sum a0 SheepInfo) (Sum PigInfo SheepInfo)
--     • In the expression: First elmo' :: Animal'
--       In an equation for ‘elmo’: elmo = First elmo' :: Animal'
-- *Main>
-- *Main> :t First (Second (SheepInfo "Baaaa" 5 5))
-- First (Second (SheepInfo "Baaaa" 5 5)) :: Sum (Sum a SheepInfo) b
-- *Main> :i Animal'
-- type Animal' = Sum CowInfo (Sum PigInfo SheepInfo)
--   	-- Defined at farmhouse.hs:74:1

-- 
-- trivialValue :: GuessWhat
-- trivialValue = Chickenbutt
--
-- idInt :: Id Integer
-- idInt = MkId 10
--
-- idIdentity :: Id (a -> a) -- is this supposed to work on it's own??
-- idIdentity = MkId $ \x -> x
--
-- type Awesome = Bool
-- type Name = String
--
-- person :: Product Name Awesome
-- person = Product "Simon" True

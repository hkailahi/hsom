data BinaryTree a =
    Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Ord, Show)

-- Note to self: FP approach is a little tricky
-- It makes more sense to me to think of BinaryTree as BinaryTreeNode object in the following function type signature
-- The way it looks now seems like the last node "holds" two leaves, rather than being a "leafNode" object
-- Same ultimate effect, different way of thinking
insert' :: Ord a => a -> BinaryTree a -> BinaryTree a
insert' b Leaf = Node Leaf b Leaf
insert' b (Node left a right)
  | b == a = Node left a right
  | b < a = Node (insert' b left) a right
  | b > a = Node left a (insert' b right)

  -- ==== Example of the BinaryTree insert function ====
-- say a = 5
-- say bT = empty tree
--
-- insert 5
--
-- insert a bT
-- -> bT = Node Leaf 5 Leaf
--
-- insert 6 bT
-- -> bt =  Node Leaf 5 Leaf
-- -> insert 6 (Node Leaf 5 Leaf) -- insert b left (node left a right)
-- -> 6 > 5 -- b > a
-- -> Node Leaf 5 (insert 6 Leaf)
-- -> Node Leaf 5 (Node Leaf 6 Leaf) -- complete. New tree holding 5 and 6

--- =================== Exercise: Implement map function for BinaryTree ======================== ---

mapTree :: (a -> b) -> BinaryTree a -> BinaryTree b
mapTree _ Leaf = Leaf
mapTree f (Node left a right) =
  Node (mapTree f left) (f a) (mapTree f right)  -- just recurse and apply the function until we hit leaves

testTree' :: BinaryTree Integer
testTree' =
  Node (Node Leaf 3 Leaf) 1 (Node Leaf 4 Leaf)

mapExpected =
  Node (Node Leaf 4 Leaf) 2 (Node Leaf 5 Leaf)

-- acceptance test for mapTree
mapOkay =
  if mapTree (+1) testTree' == mapExpected
  then print "yup okay!"
  else error "test failed!"

----------------------------------------------------------------------------------------------------

preorder :: BinaryTree a -> [a]
preorder Leaf = []
preorder (Node left root right) = root : preorder left ++ preorder right

inorder ::  BinaryTree a -> [a]
inorder Leaf = []
inorder (Node left root right) = inorder left ++ [root] ++ inorder right
-- NOTE:  Need to put root in its own list so we can concatenate

postorder :: BinaryTree a -> [a]
postorder Leaf = []
postorder (Node left root right) = postorder left ++ postorder right ++ root : []

testTreeOrder :: BinaryTree Integer
testTreeOrder = Node (Node Leaf 1 Leaf) 2 (Node Leaf 3 Leaf)

testPreorder :: IO ()
testPreorder =
  if preorder testTreeOrder == [2,1,3]
  then putStrLn "Preorder fine!"
  else putStrLn "Bad preorder"

testInorder :: IO ()
testInorder =
  if inorder testTreeOrder == [1,2,3]
  then putStrLn "Inorder fine!"
  else putStrLn "Bad inorder"

testPostorder :: IO ()
testPostorder =
  if postorder testTreeOrder == [1,3,2]
  then putStrLn "Postorder fine!"
  else putStrLn "Bad postorder"

main :: IO ()
main = do
  testPreorder
  testInorder
  testPostorder

foldTree :: (a -> b ->b) -> b -> BinaryTree a -> b
foldTree f base tree = foldr f base (inorder tree)

  -- ex. foldTree output
-- *Main> foldTree (-) 10 testTreeOrder
-- -8
-- *Main> foldTree (-) 0 testTreeOrder
-- 2
-- *Main> foldTree (+) 10 testTreeOrder
-- 16
-- *Main> foldTree (+) 0 testTreeOrder
-- 6
-- *Main> foldTree (*) 0 testTreeOrder
-- 0
-- *Main> foldTree (*) 1 testTreeOrder
-- 6

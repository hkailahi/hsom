-- solved with newtypes for types with single unary constructor
newtype Goats = Goats Int deriving (Eq, Show)
newtype Cows = Cows Int deriving (Eq, Show)

tooManyGoats :: Goats -> Bool
tooManyGoats (Goats n) = n > 42

-- *Main> let gg = Goats 42
-- *Main> tooManyGoats gg
-- False
-- *Main> tooManyGoats (Goats 123)
-- True
-- *Main> let cc = Cows 123
-- *Main> tooManyGoats cc
--
-- <interactive>:15:14: error:
--     • Couldn't match expected type ‘Goats’ with actual type ‘Cows’
--     • In the first argument of ‘tooManyGoats’, namely ‘cc’
--       In the expression: tooManyGoats cc
--       In an equation for ‘it’: it = tooManyGoats cc

class TooMany a where
  tooMany :: a -> Bool

instance TooMany Int where
  tooMany n = n > 42
-- *Main> tooMany (42 :: Int)
-- False
-- *Main> tooMany (123 :: Int)
-- True

newtype Goatzz = Goatzz Int deriving Show

instance TooMany Goatzz where
  tooMany (Goatzz n) = n > 43
-- *Main> tooMany (Goatzz 123 :: Goatzz)
-- True
-- *Main> tooMany (Goatzz 23 :: Goatzz)
-- False


-- ========================================================
-- BELOW CODE was for demonstrating issues solved after introducing newtype

-- data Goats = Goats Int deriving (Eq, Show)
--
-- tooManyGoats :: Int -> Bool
-- tooManyGoats n = n > 42
  -- example of above in prelude
-- *Main> tooManyGoats 1
-- False
-- *Main> tooManyGoats 213
-- True
-- *Main> let gg = Goats 42
-- *Main> tooManyGoats gg
--
-- <interactive>:5:14: error:
--     • Couldn't match expected type ‘Int’ with actual type ‘Goats’
--     • In the first argument of ‘tooManyGoats’, namely ‘gg’
--       In the expression: tooManyGoats gg
--       In an equation for ‘it’: it = tooManyGoats gg
-- *Main> tooManyGoats (gg :: Int)
--
-- <interactive>:6:15: error:
--     • Couldn't match expected type ‘Int’ with actual type ‘Goats’
--     • In the first argument of ‘tooManyGoats’, namely ‘(gg :: Int)’
--       In the expression: tooManyGoats (gg :: Int)
--       In an equation for ‘it’: it = tooManyGoats (gg :: Int)
-- *Main> gg
-- Goats 42

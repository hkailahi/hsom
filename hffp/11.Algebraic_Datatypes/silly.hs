data Silly a b c d = MkSilly a b c d deriving Show

  -- ex. silly outputs
-- *Main> :k Silly
-- Silly :: * -> * -> * -> * -> *
-- *Main> :k Silly Int
-- Silly Int :: * -> * -> * -> *
-- *Main> :k Silly Int String
-- Silly Int String :: * -> * -> *
-- *Main> :k Silly Int String Bool
-- Silly Int String Bool :: * -> *
-- *Main> :k Silly Int String Bool String
-- Silly Int String Bool String :: *
-- *Main> :k (,,,)
-- (,,,) :: * -> * -> * -> * -> *
-- *Main> :k (Int, String, Bool, String)
-- (Int, String, Bool, String) :: *

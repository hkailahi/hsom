-- data Expr =
--     Number Int
--   | Add Expr Expr
--   | Minus Expr
--   | Mult Expr Expr
--   | Divide Expr Expr


-- equivalent to top, stricter interpretation of normal form or "sum of products"
-- (not in lazy fp sense, more like "closer to the word")
type Number = Int
type Add = (Expr, Expr)
type Minus = Expr
type Mult = (Expr, Expr)
type Divide = (Expr, Expr)

type Expr =
  Either Number
    (Either Add
      (Either Minus
        (Either Mult Divide)))


-- todo DOESN'T WORK, WHY??

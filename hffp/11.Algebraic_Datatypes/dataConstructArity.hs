-- nullary
data Example0 =
  Example0 deriving (Eq, Show)

-- unary
data Example1 =
  Example1 Int deriving (Eq, Show)

-- product of Int and String
data Example2 =
  Example2 Int String deriving (Eq, Show)

  -- ex. dataConstructArity outputi

-- *Main> Example0
-- Example0
-- *Main> Example1 10
-- Example1 10
-- *Main> Example1 10 = Example1 42
-- *Main> Example1 10 == Example1 42
-- False
-- *Main> Example2 10 "Flappity Bat" == Example2 10 "NotCom"
-- False
-- *Main>

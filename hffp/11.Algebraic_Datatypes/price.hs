data Price = Price Integer deriving (Eq, Show)
-- type constructor
-- =
-- data constructor
-- type arguement

data Manufacturer =
              Mini
            | Mazda
            | Tata
            deriving (Eq, Show)
-- one type constructor
-- =
-- three data constructors

data Airline =
          PapuAir
        | CatapultsR'Us
        | TakeYourChancesUnited
        deriving (Eq, Show)
-- one type constructor
-- =
-- three data constructors

data Vehicle = Car Manufacturer Price
             | Plane Airline
             deriving (Eq, Show)
-- one type constructor
-- =
-- two data constructors
-- three type arguements
  -- two to Car
  -- one to Plane

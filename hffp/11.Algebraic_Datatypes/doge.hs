data PugType = PugData -- type constructor is a type constant, data constructor is a constant value

data HuskyType a = HuskyData -- type constructor takes a single parmetrically polymorphic arguement that isn't used
-- in data constructor and is thus the type arguement is "phantom", data constructor is a constant value

data DogueDeBordeaux doge = DogueDeBordeaux doge -- doge in data constructor isn't a value yet, rather a definition
-- for how to construct a value of that type


------------------------------------------------------------------

myPug = PugData :: PugType

myHusky :: HuskyType a
myHusky = HuskyData

myOtherHusky :: Num a => HuskyType a
myOtherHusky = HuskyData

myOtherOtherHusky :: HuskyType [[[[[[Int]]]]]]
myOtherOtherHusky = HuskyData
-- no witness to contrary             ^

myDoge :: DogueDeBordeaux Int
myDoge = DogueDeBordeaux 10

-- doesn't work for obvious reasons
-- badDoge :: DogueDeBordeaux String
-- badDoge = DogueDeBordeaux 10

data Doggies a =
    Husky a
  | Mastiff a
  deriving (Eq, Show)

-- type constructor awaiting arguements // doesn't work
-- Doggies

  -- ex. doge output
-- *Main> :k Doggies
-- Doggies :: * -> *
-- *Main> :t Husky
-- Husky :: a -> Doggies a

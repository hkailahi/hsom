# Book Club Chapter 1

## Prior Notes, Prep, and Recap

## 1.1 and 1.2
##### lambda calculus
The lambda calculus was devised in the 1930s by Alonzo Church as a way to formalize the concept of effective computability, serving the same purpose as the Turing machine but de-emphasizing the “machine” part. The goal of formalizing a model of computation is to determine which problems, or classes of problems, can be solved.
- Pure FP is just the lambda calculus.

##### function
A function is a relation between two sets, a set of inputs and a set of outputs. The function itself defines and represents the relationship. The set of inputs is referred to as the domain and the set of outputs the codomain.
* Here a set is a collection of unique values.
* You cannot return two different results for the same input; the notion doesn’t even make sense. This accords nicely with our understanding of the uniqueness of values in sets.
* a mapping of a unique set of inputs to a unique set of outputs

##### functional programming
Functional programming is a computer programming paradigm that relies on definition and application of functions as the basis for programs (contrast this to the sequential instructions you find in imperative programs).
- The essence of functional programming is that programs are a combination of expressions to be evaluated

##### expressions
Expressions are a superset that includes concrete values, variables, and functions. Functions have a more specific definition: they are expressions that are applied to an argument and, once applied, can be reduced or evaluated. In Haskell, and in FP more generally, functions are first-class: they can be used as values or arguments to yet more functions.

## 1.3
 The lambda calculus has three basic components: expressions, variables, and abstractions.

- An expression can be a variable name, an abstraction (that is, a lambda itself), a function application (a variable or lambda applied to an argument), or some combination of all these. The simplest expression is a single variable. The variable has no meaning or value. It just gives a name to a potential input to a function.

Functions are a way of describing an operation that, given an in-put, produces a specific output. The head of the function is a λ (lambda) followed by a variable name that is the parameter or argument of that function. The body of the function is another expression

λ x . x
- identity function ( f(x) = x )
- λ x = head
- first x is argument lambda takes

##### example of applying a lambda to a function
λ x . x (2)
* // eliminate the head
* // substitute 2 for each x in the body

2

## 1.4

##### function application
Function application consists of applying the abstraction to another expression.

##### beta-reduction
When you apply the function, you substitute the input expression for all instances of bound variables within the abstraction, a process known as beta-reduction. At the same time you eliminate the head that binds the variables. When there are no more heads, or lambdas, left, you’re done.

##### computation
A computation therefore consists of an initial lambda expression (or two, if you want to separate the function and its input) plus a finite sequence of lambda terms, each deduced from the preceding term by one application of beta-reduction.

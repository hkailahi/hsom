myTail :: [a] -> [a]
myTail []       = []
myTail (_:xs) = xs

safeTail :: [a] -> Maybe [a]
safeTail []       = Nothing
safeTail (x:[])   = Nothing -- if a list only has one value, it doesn't have a tail
safeTail (_:xs) = Just xs

  -- ex. myAppendage output
-- *Main> myTail [1..10]
-- [2,3,4,5,6,7,8,9,10]
-- myTail . filter even $ [1..100]
-- [4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80,82,84,86,88,90,92,94,96,98,100]
-- *Main> myTail "Phello world"
-- "hello world"
-- *Main> safeTail []
-- Nothing
-- *Main> safeTail [1]
-- Nothing
-- *Main> safeTail . filter even $ [1..100]
-- Just [4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80,82,84,86,88,90,92,94,96,98,100]

-- Prelude> :i Fractional
-- class Num a => Fractional a where
--   (/) :: a -> a -> a
--   recip :: a -> a
--   fromRational :: Rational -> a
--   {-# MINIMAL fromRational, (recip | (/)) #-}
--   	-- Defined in ‘GHC.Real’
-- instance Fractional Float -- Defined in ‘GHC.Float’
-- instance Fractional Double -- Defined in ‘GHC.Float’

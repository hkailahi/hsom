data DayOfWeek = Mon | Tue | Weds | Thu | Fri | Sat | Sun deriving (Ord, Show)

-- day of week and numerical day of month
data Date = Date DayOfWeek Int

-- instance Ord DayOfWeek where
--   compare Fri Fri = EQ
--   compare Fri _   = GT
--   compare _ Fri   = LT
--   compare _ _     = EQ

instance Eq DayOfWeek where
  (==) Mon Mon    = True
  (==) Tue Tue    = True
  (==) Weds Weds  = True
  (==) Thu Thu    = True
  (==) Fri Fri    = True
  (==) Sat Sat    = True
  (==) Sun Sun    = True
  (==) _ _        = False

instance Eq Date where
  (==) (Date weekday monthNum) (Date weekday' monthNum') = weekday == weekday' && monthNum == monthNum'

  -- ex. DayOfWeek output
-- *Main> Date Thu 10 == Date Thu 10
-- True
-- *Main> Date Thu 10 == Date Thu 11
-- False
-- *Main> Date Thu 10 == Date Weds 11
-- False

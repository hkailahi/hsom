-- Prelude> :i Show
-- class Show a where
--   showsPrec :: Int -> a -> ShowS
--   show :: a -> String
--   showList :: [a] -> ShowS
--   {-# MINIMAL showsPrec | show #-}
--   	-- Defined in ‘GHC.Show’
-- instance Show a => Show [a] -- Defined in ‘GHC.Show’
-- instance Show Word -- Defined in ‘GHC.Show’
-- instance Show Ordering -- Defined in ‘GHC.Show’
-- instance Show a => Show (Maybe a) -- Defined in ‘GHC.Show’
-- instance Show Integer -- Defined in ‘GHC.Show’
-- instance Show Int -- Defined in ‘GHC.Show’
-- instance Show Char -- Defined in ‘GHC.Show’
-- instance Show Bool -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f, Show g,
--           Show h, Show i, Show j, Show k, Show l, Show m, Show n, Show o) =>
--          Show (a, b, c, d, e, f, g, h, i, j, k, l, m, n, o)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f, Show g,
--           Show h, Show i, Show j, Show k, Show l, Show m, Show n) =>
--          Show (a, b, c, d, e, f, g, h, i, j, k, l, m, n)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f, Show g,
--           Show h, Show i, Show j, Show k, Show l, Show m) =>
--          Show (a, b, c, d, e, f, g, h, i, j, k, l, m)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f, Show g,
--           Show h, Show i, Show j, Show k, Show l) =>
--          Show (a, b, c, d, e, f, g, h, i, j, k, l)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f, Show g,
--           Show h, Show i, Show j, Show k) =>
--          Show (a, b, c, d, e, f, g, h, i, j, k)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f, Show g,
--           Show h, Show i, Show j) =>
--          Show (a, b, c, d, e, f, g, h, i, j)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f, Show g,
--           Show h, Show i) =>
--          Show (a, b, c, d, e, f, g, h, i)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f, Show g,
--           Show h) =>
--          Show (a, b, c, d, e, f, g, h)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f,
--           Show g) =>
--          Show (a, b, c, d, e, f, g)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e, Show f) =>
--          Show (a, b, c, d, e, f)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d, Show e) =>
--          Show (a, b, c, d, e)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c, Show d) => Show (a, b, c, d)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b, Show c) => Show (a, b, c)
--   -- Defined in ‘GHC.Show’
-- instance (Show a, Show b) => Show (a, b) -- Defined in ‘GHC.Show’
-- instance Show () -- Defined in ‘GHC.Show’
-- instance (Show b, Show a) => Show (Either a b)
--   -- Defined in ‘Data.Either’
-- instance Show Float -- Defined in ‘GHC.Float’
-- instance Show Double -- Defined in ‘GHC.Float’

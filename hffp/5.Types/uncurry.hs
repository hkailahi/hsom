nonsense :: Bool -> Integer
nonsense True = 805
nonsense False = 31337 -- leet lol

typicalCurriedFunction :: Integer -> Bool -> Integer
typicalCurriedFunction i b = i + (nonsense b)

anonymous :: Integer -> Bool -> Integer
anonymous = \i b -> i + (nonsense b)

anonymousAndManuallyNested :: Integer -> Bool -> Integer
anonymousAndManuallyNested = \i -> \b -> i + (nonsense b)

  -- ex. uncurry output
-- *Main> typicalCurriedFunction 10 False
-- 31347
-- *Main> anonymous 10 False
-- 31347
-- *Main> anonymousAndManuallyNested 10 False
-- 31347

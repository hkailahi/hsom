module Adder where
  addStuff :: Integer -> Integer -> Integer
  addStuff a b = a + b + 5

  -- ex. Adder output
-- *Adder> :t addStuff
-- addStuff :: Integer -> Integer -> Integer
-- *Adder> let addTen = addStuff 5
-- *Adder> let fifteen = addTen 5
-- *Adder> fifteen
-- 15
-- *Adder> addTen 15
-- 25
-- *Adder> addStuff 5 5
-- 15

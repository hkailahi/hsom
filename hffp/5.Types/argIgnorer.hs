module ArgIgnorer where
  funcIgnoresArgs :: a -> a -> a -> String
  funcIgnoresArgs x y z = "Blah"

  -- ex. ArgIgnorer output
-- *ArgIgnorer> :t funcIgnoresArgs
-- funcIgnoresArgs :: a -> a -> a -> String
-- *ArgIgnorer> :t funcIgnoresArgs (1 :: Integer)
-- funcIgnoresArgs (1 :: Integer) :: Integer -> Integer -> String

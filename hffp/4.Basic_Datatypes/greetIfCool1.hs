module GreetIfCool1 where
  greetIfCool :: String -> IO ()
  greetIfCool coolness =
    if cool
      then putStrLn "eyyyy. Looking what is, cooking good"
    else
      putStrLn "nah gtfo m'face fam"
    where cool = coolness == "downright frosty yo"

    -- ex. greetIfCool output
-- *GreetIfCool1> greetIfCool "downright frosty yo"
-- eyyyy. Looking what is, cooking good
-- *GreetIfCool1> greetIfCool "notice me"
-- nah gtfo m'face fam
-- *GreetIfCool1>

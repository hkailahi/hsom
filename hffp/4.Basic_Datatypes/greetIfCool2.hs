module GreetIfCool2 where
  greetIfCool :: String -> IO ()
  greetIfCool coolness =
    if cool coolness
      then putStrLn "eyyyy. Looking what is, cooking good"
    else
      putStrLn "nah gtfo m'face fam"
    where cool v = v == "downright frosty yo"

    -- ex. greetIfCool output
-- *GreetIfCool2> greetIfCool "downright frosty yo"
-- eyyyy. Looking what is, cooking good
-- *GreetIfCool2> greetIfCool "notice me"
-- nah gtfo m'face fam
-- *GreetIfCool2>

module Global where
  topLevelFunction :: Integer -> Integer
  topLevelFunction x = x + woot + topLevelValue
    where woot :: Integer
          woot = 10

  topLevelValue :: Integer
  topLevelValue = 5

-- why? point of this file?
-- show that woot is "locally defined" var
-- while topLevelValue and topLevelFunction are "globally defined" var/func

  -- ex. topLevelFunction/topLevelValue output
-- *Global> topLevelFunction 1
-- 16
-- *Global> topLevelFunction 100
-- 115
-- *Global> topLevelValue
-- 5
-- *Global> woot
--
-- <interactive>:19:1: error: Variable not in scope: woot
-- *Global>

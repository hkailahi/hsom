module Print3 where
  myGreeting :: String
  myGreeting = "hello" ++ " world!"

  hello :: String
  hello = "hello"

  world :: String
  world = "world"

  main :: IO ()
  main = do
    putStrLn myGreeting
    putStrLn secondGreeting
    where secondGreeting = concat [hello, " ", world, "!"]


    --  ex. main output
-- *Print3> main
-- hello world!
-- hello world!
-- *Print3> myGreeting
-- "hello world!"
-- *Print3>

module FunctionWithLet where
  printInc2 n = let plusTwo = n + 2
                in print plusTwo
  -- turns into
  printInc2' n = (\plusTwo -> print plusTwo) (n + 2)
  -- let expressions do not directly translate to lambda calculus
  --  in this way if there are free variables

safeTail :: [a] -> Maybe [a]
safeTail []     = Nothing
safeTail (x:[]) = Nothing
safeTail (_:xs) = Just xs

  -- ex. safeTail output
-- Main> :t safeTail [1..(10 :: Int)]
-- safeTail [1..(10 :: Int)] :: Maybe [Int]
-- *Main> :t safeTail [1..10 :: Int]
-- safeTail [1..10 :: Int] :: Maybe [Int]
-- *Main> :t safeTail [(1 :: Int)..10]
-- safeTail [(1 :: Int)..10] :: Maybe [Int]
-- *Main> :t safeTail [(1 :: Int)..(10 :: Int)]
-- safeTail [(1 :: Int)..(10 :: Int)] :: Maybe [Int]
--
-- *Main> :t safeTail
-- safeTail :: [a] -> Maybe [a]
-- *Main> :t safeTail 10
-- safeTail 10 :: Num [a] => Maybe [a]
-- *Main> :t safeTail "abc"
-- safeTail "abc" :: Maybe [Char]

  -- playing with Kinds in Prelude
-- *Main> data Trivial = Trivial
-- *Main> :k Trivial
-- Trivial :: *
-- *Main> data Unary a = Unary a
-- *Main> :k Unary
-- Unary :: * -> *
-- *Main> :k Unary Int
-- Unary Int :: *
-- *Main> data TwoArgs a b = TwoArgs a b
-- *Main> :k TwoArgs
-- TwoArgs :: * -> * -> *
-- *Main> :k TwoArgs Int
-- TwoArgs Int :: * -> *
-- *Main> :k TwoArgs Int String
-- TwoArgs Int String :: *

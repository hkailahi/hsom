type Name = String
type Age = Integer

data Person = Person Name Age deriving Show

data PersonInvalid = NameEmpty | AgeTooLow deriving (Eq, Show)

-- mkPerson :: Name -> Age -> Maybe Person
-- mkPerson name age
--   | name /= "" && age >= 0 = Just $ Person name age
--   | otherwise              = Nothing
  -- ex. mkPerson output
-- *Main> mkPerson "" 160
-- Nothing
-- *Main> mkPerson "John" 0
-- Just (Person "John" 0)
-- *Main> mkPerson "John" -1
--
-- <interactive>:12:1: error:
--     • No instance for (Num (Age -> Maybe Person))
--         arising from a use of ‘-’
--         (maybe you haven't applied a function to enough arguments?)
--     • In the expression: mkPerson "John" - 1
--       In an equation for ‘it’: it = mkPerson "John" - 1
-- *Main> mkPerson "John" (-1)
-- Nothing
-- *Main> mkPerson "John" 120
-- Just (Person "John" 120)
-- *Main>

mkPerson2 :: Name -> Age -> Either PersonInvalid Person
mkPerson2 name age
  | name /= "" && age >= 0 = Right $ Person name age
  | name == ""             = Left NameEmpty
  | otherwise              = Left AgeTooLow
  -- ex. mkPerson2 output
-- *Main> mkPerson2 "" 0
-- Left NameEmpty
-- *Main> mkPerson2 "John" (-2)
-- Left AgeTooLow
-- *Main> mkPerson2 "John" 100
-- Right (Person "John" 100)
-- *Main> :t mkPerson2 "Djal" 5
-- mkPerson2 "Djal" 5 :: Either PersonInvalid Person
-- *Main> mkPerson2 "Djal" 5
-- Right (Person "Djal" 5)

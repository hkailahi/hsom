  -- Nullary and unary data constructor play in prelude
-- Prelude> data Trivial = Trivial deriving Show
-- Prelude> Trivial
-- Trivial
-- Prelude> Trivial 12
--
-- <interactive>:4:1: error:
--     • Couldn't match expected type ‘Integer -> t’
--                   with actual type ‘Trivial’
--     • The function ‘Trivial’ is applied to one argument,
--       but its type ‘Trivial’ has none
--       In the expression: Trivial 12
--       In an equation for ‘it’: it = Trivial 12
--     • Relevant bindings include it :: t (bound at <interactive>:4:1)


-- Prelude> data UnaryC = UnaryC Int deriving Show
-- Prelude> :t UnaryC
-- UnaryC :: Int -> UnaryC
-- Prelude> :t UnaryC 10
-- UnaryC 10 :: UnaryC
-- Prelude> UnaryC 10
-- UnaryC 10


-- Prelude> data Unary a = Unary a deriving Show
-- Prelude> :t Unary
-- Unary :: a -> Unary a
-- Prelude> :t Unary 10
-- Unary 10 :: Num a => Unary a
-- Prelude> :t Unary "blah"
-- Unary "blah" :: Unary [Char]
-- Prelude> :i Unary
-- data Unary a = Unary a 	-- Defined at <interactive>:13:1
-- instance [safe] Show a => Show (Unary a)
--   -- Defined at <interactive>:13:33
-- Prelude> :t (Unary id)
-- (Unary id) :: Unary (a -> a)

-- Prelude> fmap Just [1..10]
-- [Just 1,Just 2,Just 3,Just 4,Just 5,Just 6,Just 7,Just 8,Just 9,Just 10]
-- Prelude> ['a'..'z']
-- "abcdefghijklmnopqrstuvwxyz"
-- [Just 'a',Just 'b',Just 'c',Just 'd',Just 'e',Just 'f',Just 'g',Just 'h',Just 'i',Just 'j',Just 'k',Just 'l',Just 'm',Just 'n',Just 'o',Just 'p',Just 'q',Just 'r',Just 's',Just 't',Just 'u',Just 'v',Just 'w',Just 'x',Just 'y',Just 'z']

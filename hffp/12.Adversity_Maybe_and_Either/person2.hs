type Name = String
type Age = Integer
type ValidatePerson a = Either [PersonInvalid] a

data Person = Person Name Age deriving Show

data PersonInvalid = NameEmpty | AgeTooLow deriving (Eq, Show)

mkPerson :: Name -> Age -> ValidatePerson Person
mkPerson name age = mkPerson' (nameOkay name) (ageOkay age)
-- later we will replace the above mkPerson with the following:
-- mkPerson :: Name -> Age -> ValidatePerson [PersonInvalid] Person
-- mkPerson name age = liftA2 (nameOkay name) (ageOkay age)

mkPerson' :: ValidatePerson Name -> ValidatePerson Age -> ValidatePerson Person
mkPerson' (Right nameOk) (Right ageOk) = Right (Person nameOk ageOk)
mkPerson' (Left badName) (Left badAge) = Left (badName ++ badAge)
mkPerson' (Left badName) _             = Left badName
mkPerson' _ (Left badAge)              = Left badAge

nameOkay :: Name -> Either [PersonInvalid] Name
nameOkay name = case name /= "" of
  True -> Right name
  False -> Left [NameEmpty]

ageOkay :: Age -> Either [PersonInvalid] Age
ageOkay age = case age > 0 of
  True -> Right age
  False -> Left [AgeTooLow]

  -- example mkPerson output
-- *Main> mkPerson "" 0
-- Left [NameEmpty,AgeTooLow]
-- *Main> mkPerson "John" 0
-- Left [AgeTooLow]
-- *Main> mkPerson "" 10
-- Left [NameEmpty]
-- *Main> mkPerson "John" 10
-- Right (Person "John" 10)

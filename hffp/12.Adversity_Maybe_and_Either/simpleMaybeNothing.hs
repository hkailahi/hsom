ifEvenAdd2 :: Integer -> Maybe Integer
ifEvenAdd2 n = if even n then Just (n+2) else Nothing

  -- ex. ifEvenAdd2 output
-- *Main> ifEvenAdd2 10
-- Just 12
-- *Main> ifEvenAdd2 1123123
-- Nothing
-- *Main> ifEvenAdd2 1123122
-- Just 1123124

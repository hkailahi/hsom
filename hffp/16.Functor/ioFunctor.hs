getInt :: IO Int
getInt = fmap read getLine

  -- ex getInt output
-- *Main> getInt
-- 100
-- 100
-- *Main> let getInt = 10 :: Int
-- *Main> const () getInt
-- ()
-- *Main> getInt
-- 10

meTooIsm :: IO String
meTooIsm = do
  input <- getLine
  return (input ++ "and me too!")

bumpIt :: IO Int
bumpIt = do
  intVal <- getInt
  return (intVal + 1)

  -- ex meTooIsm and bumpIt output
-- *Main> meTooIsm
-- hello
-- "helloand me too!"
-- *Main> meTooIsm
-- "Hello"
-- "\"Hello\"and me too!"
-- *Main> meTooIsm
-- hello
-- "hello and me too!"
-- *Main> bumpIt
-- 1000
-- 1001

{-# LANGUAGE ViewPatterns #-}

module CheckFunctorLaws where

import Test.QuickCheck
import Test.QuickCheck.Function

  -- Functor Laws
-- Identity:      fmap id = id
-- Composition:   fmap (p . q) = (fmap p) . (fmap q)

functorIdentity :: (Functor f, Eq (f a)) => f a -> Bool
functorIdentity f = fmap id f == f

functorCompose :: (Eq (f c), Functor f) => (a -> b) -> (b -> c) -> f a -> Bool
functorCompose f g x = (fmap g (fmap f x)) == (fmap (g . f) x)

functorCompose' :: (Eq (f c), Functor f) => f a -> Fun a b -> Fun b c -> Bool
functorCompose' x (Fun _ f) (Fun _ g) = (fmap (g . f) x) == (fmap g . fmap f $ x)


  -- example CheckFunctorLaws output
-- *CheckFunctorLaws> quickCheck $ \x -> functorIdentity (x :: [Int])
-- +++ OK, passed 100 tests.
-- *CheckFunctorLaws> let li x = functorCompose (+1) (*2) (x :: [Int])
-- *CheckFunctorLaws> quickCheck li
-- +++ OK, passed 100 tests.
-- *CheckFunctorLaws> type IntToInt = Fun Int Int
-- *CheckFunctorLaws> type IntFC = [Int] -> IntToInt -> IntToInt -> Bool
-- *CheckFunctorLaws> quickCheck (functorCompose' :: IntFC)
-- +++ OK, passed 100 tests.

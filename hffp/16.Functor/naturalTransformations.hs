{-# LANGUAGE RankNTypes #-}


-- nat :: (f -> g) -> f a -> g a
-- ^ the above function is impossible becayse we can't have higher-kinded types as arguement types to the function type

type Nat f g = forall a. f a -> g a

maybeToList :: Nat Maybe []
maybeToList Nothing = []
maybeToList (Just a) = [a]

-- errorProneMtl :: Nat Maybe []
-- errorProneMtl Nothing = []
-- errorProneMtl (Just a) = [a+1]  -- we are not allowed to change contents like what is being done here

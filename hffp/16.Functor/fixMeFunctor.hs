data FixMePls a =
    FixMe
  | Pls a
  deriving (Eq, Show)

instance Functor FixMePls where
  fmap _ FixMe = FixMe
  fmap f (Pls a) = Pls (f a)


  -- ex. fixMeFunctor output
-- Main> fmap (++ " World!") (Pls "Hello")
-- Pls "Hello World!"
-- *Main> fmap (++ " World!") (FixMe "Hello")
--
-- <interactive>:34:22: error:
--   • Couldn't match expected type ‘[Char] -> f [Char]’
--                 with actual type ‘FixMePls a0’
--   • The function ‘FixMe’ is applied to one argument,
--     but its type ‘FixMePls a0’ has none
--     In the second argument of ‘fmap’, namely ‘(FixMe "Hello")’
--     In the expression: fmap (++ " World!") (FixMe "Hello")
--   • Relevant bindings include
--       it :: f [Char] (bound at <interactive>:34:1)
-- *Main> fmap (++ " World!") (FixMe)
-- FixMe
-- *Main> fmap (+1) (Pls 1)
-- Pls 2
-- *Main> fmap (+1) (FixMe)
-- FixMe
-- *Main> fmap (+10000) (FixMe)
-- FixMe

  -- messing with list (maybe (string)) functor. Functors are stacked
-- *Main> let lms = [Just "Ave", Nothing, Just "woohoo"]
-- *Main> lms
-- [Just "Ave",Nothing,Just "woohoo"]
-- *Main> let replaceWithP = const 'p'
-- *Main> replaceWithP lms
-- 'p'
-- *Main> lms
-- [Just "Ave",Nothing,Just "woohoo"]
-- *Main> :i const
-- const :: a -> b -> a 	-- Defined in ‘GHC.Base’
-- *Main> fmap replaceWithP lms                                     -- replaces List elements -- fmap -> List to 'p'
-- "ppp"
-- *Main> (fmap . fmap) replaceWithP lms                            -- replaces m in Maybe m  -- fmap . fmap -> Maybe in List to 'p'
-- [Just 'p',Nothing,Just 'p']
-- *Main> (fmap . fmap . fmap) replaceWithP lms                     -- replaces characters    -- fmap . fmap . fmap -> Char in Maybe in List to 'p'
-- [Just "ppp",Nothing,Just "pppppp"]
-- *Main> (fmap . fmap . fmap . fmap) replaceWithP lms
--
-- <interactive>:59:42: error:
--     • Couldn't match type ‘Char’ with ‘f b0’
--       Expected type: [Maybe [f b0]]
--         Actual type: [Maybe [Char]]
--     • In the second argument of ‘fmap . fmap . fmap . fmap’, namely
--         ‘lms’
--       In the expression: (fmap . fmap . fmap . fmap) replaceWithP lms
--       In an equation for ‘it’:
--           it = (fmap . fmap . fmap . fmap) replaceWithP lms
--     • Relevant bindings include
--         it :: [Maybe [f Char]] (bound at <interactive>:59:1)
-- *Main>

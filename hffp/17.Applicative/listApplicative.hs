-- f ~ []


  -- Introducing the List Applicative
-- (<*>) :: f (a -> b) -> f a -> f b
-- (<*>) :: [] (a -> b) -> [] a -> [] b
-- (<*>) :: [(a -> b)] -> [a] -> [b]
  --
-- pure :: a -> f a
-- pure :: a -> [] a

  -- List Functor vs List Applicative
-- Prelude Data.Monoid> fmap (2^) [1..3]
-- [2,4,8]
-- Prelude Data.Monoid> fmap (^2) [1..3]
-- [1,4,9]
-- Prelude Data.Monoid> [(2^),(^2)] <*> [1..3]
-- [2,4,8,1,4,9]
-- Prelude Data.Monoid> [(2^),(^2),(2^)] <*> [1..3]
-- [2,4,8,1,4,9,2,4,8]
-- Prelude Data.Monoid> [(2^),(^2)] <*> [1..5]
-- [2,4,8,16,32,1,4,9,16,25]

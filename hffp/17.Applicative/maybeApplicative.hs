import Data.Monoid
import Control.Applicative

validateLength :: Int -> String -> Maybe String
validateLength maxLen s =
  if (length s) > maxLen
    then Nothing
    else Just s

newtype Name = Name String deriving (Eq, Show)
newtype Address = Address String deriving (Eq, Show)

mkName :: String -> Maybe Name
mkName s = fmap Name $ validateLength 25 s

mkAddress :: String -> Maybe Address
mkAddress a = fmap Address $ validateLength 100 a

data Person = Person Name Address deriving (Eq, Show)

mkPerson :: String -> String -> Maybe Person
mkPerson n a =
  Person <$> mkName n <*> mkAddress a -- equivalent to liftA2 Person (mkName n) (mkAddress a)

mkPerson' :: String -> String -> Maybe Person
mkPerson' n a =
  liftA2 Person (mkName n) (mkAddress a)

  -- ex. mkPerson and liftA2 output
-- *Main Data.Monoid Control.Applicative> :t liftA2 Person (mkName "John") (mkAddress "123")
-- liftA2 Person (mkName "John") (mkAddress "123") :: Maybe Person
-- *Main Data.Monoid Control.Applicative> let bb = mkPerson "Joe" "M St"
-- *Main Data.Monoid Control.Applicative> bb
-- Just (Person (Name "Joe") (Address "M St"))
-- *Main Data.Monoid Control.Applicative> let aa = mkPerson "Joe2" "M2 St"
-- *Main Data.Monoid Control.Applicative> aa
-- Just (Person (Name "Joe2") (Address "M2 St"))
-- *Main Data.Monoid Control.Applicative>

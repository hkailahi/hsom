import Data.Monoid

type Verb = String
type Adjective = String
type Adverb = String
type Noun = String
type Exclamation = String

  -- Ad lib used in madlibbin function
-- "__exclamation___! he said __adverb__ as he
-- jumped into his car __noun__ and drove
-- off with his __adjective__ wife."

madlibbin :: Exclamation -> Adverb -> Noun -> Adjective -> String
madlibbin e adv noun adj =
  e <> "! he said " <>
  adv <> " as he jumped into his car "  <>
  noun <> " and drove off with his " <>
  adj <> " wife."

-- Exercise: rewrite madlibbin using mconcat
madlibbin2 :: Exclamation -> Adverb -> Noun -> Adjective -> String
madlibbin2 e adv noun adj =
  mconcat [e, "! he said ", adv, " as he jumped into his car ", noun, " and drove off with his ", adj, " wife."]

  -- ex. madlibs output
-- *Main> madlibbin "WTF" "angrily" "transformer" "deranged"
-- "WTF! he said angrily as he jumped into his car transformer and drove off with his deranged wife."
-- *Main> madlibbin2 "WTF" "angrily" "transformer" "deranged"
-- "WTF! he said angrily as he jumped into his car transformer and drove off with his deranged wife."

import Data.Monoid
import Test.QuickCheck

-- checking that (+) and (*) are associative
-- 1 + (2 + 3) == (1 + 2) + 3
-- 4 * (5 * 6) == (4 * 5) * 6

-- lambda
-- \ f a b c ->
--   f a (f b c) == f (f a b) c

-- lambda infix
-- \ (<>) a b c ->
--   a <> (b <> c) == (a <> b) <> c

-- associtive
asc :: Eq a => (a -> a -> a) -> a -> a -> Bool
asc (<>) a b c =
  a <> (b <> c) == (a <> b) <> c

-- associatve with QuickCheck
monoidAssoc :: (Eq m, Monoid m) => m -> m -> m -> Bool
monoidAssoc a b c = (a <> (b <> c)) == ((a <> b) <> c)

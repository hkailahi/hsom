-- (**) Pack consecutive duplicates of list elements into sublists.
--
-- If a list contains repeated elements they should be placed in separate sublists.
--
-- Example:
--
-- * (pack '(a a a a b c c a a d e e e e))
-- ((A A A A) (B) (C C) (A A) (D) (E E E E))

import Data.List

pack :: (Eq a) => [a] -> [[a]]
pack (x:xs) = let (first,rest) = span (==x) xs
             in (x:first) : pack rest
pack [] = []

-- https://wiki.haskell.org/99_questions/Solutions/9

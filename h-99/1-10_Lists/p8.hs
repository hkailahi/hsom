-- (**) Eliminate consecutive duplicates of list elements.
--
-- If a list contains repeated elements they should be replaced with a single
-- copy of the element. The order of the elements should not be changed.
--
-- Example:
--
-- * (compress '(a a a a b c c a a d e e e e))
-- (A B C A D E)
-- Example in Haskell:
--
-- > compress "aaaabccaadeeee"
-- "abcade"

-- import Data.List.group

compress :: (Eq a) => [a] -> [a]
compress (x:ys@(y:_))
    | x == y    = compress ys
    | otherwise = x : compress ys
compress ys = ys


-- compress' :: Eq a => [a] -> [a]
-- compress' = map head . group

compress'' :: Eq a => [a] -> [a]
compress'' []     = []
compress'' (x:xs) = x : (compress $ dropWhile (== x) xs)

-- https://wiki.haskell.org/99_questions/Solutions/8
